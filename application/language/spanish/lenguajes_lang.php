<?php

$lang['form_legend'] = 'Cambiar Idioma';

$lang['form_language'] = 'Selecciones su lenguaje:';
$lang['form_spanish'] = 'Español';
$lang['form_english'] = 'Inglés';
$lang['form_label_name'] = 'Ingrese su nombre:';
$lang['form_label_address'] = 'Ingrese su dirección:';
$lang['form_label_sex'] = 'Ingrese su sexo:';
$lang['form_sex'] = 'Seleccione...';
$lang['form_female'] = 'Femenino';
$lang['form_male'] = 'Masculino';
$lang['form_button'] = 'Enviar Formulario';