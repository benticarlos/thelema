<?php

require(APPPATH . 'table/class.fpdf_table.php');

class Pdf extends fpdf_table {

    public function Header() {
        $this->SetStyle("head1", "arial", "B", 8, "");
        $this->SetStyle("head2", "arial", "B", 8, "");
        $this->SetStyle("title", "", "B", 8, "");
        $this->SetY(7);
        $this->SetX(35);
        $this->MultiCellTag(0, 4, "<head1>" . iconv('UTF-8', 'windows-1252', 'Gobernación del Estado Bolivariano de Mérida') . "\n</head1><head2>" . iconv('UTF-8', 'windows-1252', "      Unidad de Gestión Energética - UGE") . "\n\n</head2><title>" . iconv('UTF-8', 'windows-1252', $GLOBALS['title']) . "</title>", 'C');
        $this->Image(APPPATH . 'table/images/logo_gober.jpg', 15, 5, 18, 0, '', '');
        $this->Image(APPPATH . 'table/images/consumo_eficiente.jpg', 240, 5, 14, 0, '', '');
        /* $this->SetFont('Arial', 'B', 12);
          $this->Ln(); */
    }

    public function Footer() {
        $this->SetY(-10);
        $this->SetFont('Arial', 'I', 8);
        $this->Cell(23, 15, date('d/m/y'), 0);
        $this->Cell(0, 10,  'Pag. '. $this->PageNo(), 0, 0, 'C');
        $this->SetFont('Times', '', 12);
    }

    public function tabla($columns, $data, $title, $tableHeader = null, $theaderType = null, $tdataType = null, $tableType = null) {
        if (is_null($theaderType))
            $theaderType = array(
                'WIDTH' => 6,
                'T_COLOR' => array(255, 255, 240),
                'T_SIZE' => 7,
                'T_FONT' => 'Arial',
                'T_ALIGN' => 'C',
                'V_ALIGN' => 'M',
                'T_TYPE' => 'B',
                'LN_SIZE' => 6,
                'BG_COLOR' => array(11, 62, 111),
                'BRD_COLOR' => array(0, 30, 30),
                'BRD_SIZE' => 0.2,
                'BRD_TYPE' => '1',
                'TEXT' => ''
            );

        if (is_null($tdataType))
            $tdataType = array(
                'T_COLOR' => array(0, 0, 0), //text color
                'T_SIZE' => 8, //font size
                'T_FONT' => 'Arial', //font family
                'T_ALIGN' => 'C', //horizontal alignment, possible values: LRC (left, right, center)
                'V_ALIGN' => 'M', //vertical alignment, possible values: TMB(top, middle, bottom)
                'T_TYPE' => '', //font type
                'LN_SIZE' => 5, //line size for one row
                'BG_COLOR' => array(255, 255, 240), //background color
                'BRD_COLOR' => array(11, 62, 111), //border color
                'BRD_SIZE' => 0.05, //border size
                'BRD_TYPE' => '1', //border type, can be: 0, 1 or a combination of: "LRTB"
            );

        if (is_null($tableType))
            $tableType = array(
                'TB_ALIGN' => 'P', //table align on page
                'L_MARGIN' => 0, //space to the left margin
                'BRD_COLOR' => array(0, 66, 118), //border color
                'BRD_SIZE' => '0.3'   //border size
            );
        $this->Open(); //Begin document
        $this->SetAutoPageBreak(true, 25); //establece el modo de salto de pagina automático ---Distancia desde la parte inferior de la página .
        $this->SetMargins(03, 30, 20); //Define los márgenes izquierdo, superior, y derecho. Por defecto, son iguales a 1 cm.
        $this->AddPage(); //Añade una nueva página al documento.--Recibe dos parametros (Orentacion, Tamaño)
        $this->AliasNbPages(); //Define un alias para el número total de páginas.---Valor por defecto: {nb}.
        $this->SetStyle("title", "arial", "", 12, "0,0,0"); //SetStyle($tag, $family, $style, $size, $color)-----------Sets current tag to specified style.
        $this->SetTextColor(118, 0, 3); //Define el color usado por el texto. Este puede ser expresado en componentes RGB o escala de grises. El método puede ser invocado antes que la primera página sea creada y el valor es retenido de página a página.
        $this->SetY(25);
        $this->MultiCellTag(0, 4, "<s1>" . $title . "</s1>", 0, 'C'); //MultiCellTag($w, $h, $pData, $border=0, $align='J', $fill=0, $pad_left=0, $pad_top=0, $pad_right=0, $pad_bottom=0, $pDataIsString = true)
        $this->Ln(1); //salto de linea.
        $this->tbInitialize($columns, true, true); ////Initialize the table class---tbInitialize($iColumnsNr = 0, $bTableHeaderDraw = true, $bTableBorderDraw = true)
        $this->tbSetTableType($tableType); //set the Table Type
        //set the Table Header
        if (!is_null($tableHeader)) {
            if (count($tableHeader) > 1) {
                $tableHead2 = array();
                foreach ($tableHeader[1] as $key => $value) {
                    $tableHead2[$key] = $theaderType;
                    foreach ($tableHeader[1][$key] as $key2 => $value2) {
                        $tableHead2[$key][$key2] = $value2;
                    }
                }
                $tableHead = $tableHead2;
                foreach ($tableHeader[0] as $key => $value) {
                    $tableHead[0][$key] = $value;
                }
                $tableHeads = array($tableHead, $tableHead2);
                $this->tbSetHeaderType($tableHeads, true);
            } else {
                $tableHead = array();
                foreach ($tableHeader[0] as $key => $value) {
                    $tableHead[$key] = $theaderType;
                    foreach ($tableHeader[0][$key] as $key2 => $value2) {
                        $tableHead[$key][$key2] = $value2;
                    }
                }
                $this->tbSetHeaderType($tableHead);
            }
        }
        //Draw the Header
        $this->tbDrawHeader();
        //Table Data Settings
        for ($i = 0; $i < $columns; $i++) {
            $dataType[$i] = $tdataType;
        }
        $this->tbSetDataType($dataType);
        foreach ($data as $key => $value) {
            foreach ($data[$key] as $key2 => $value2) {
                foreach ($data[$key][$key2] as $key3 => $value3) {
                    $tableData[$key2][$key3] = $value3;
                }
            }
            $this->tbDrawData($tableData);
        }
        //output the table data to the pdf
        $this->tbOuputData();
        //draw the Table Border
        $this->tbDrawBorder();
        $this->Output('document.pdf', 'D'); //Envía el documento a un destino dado: una cadena, un fichero local o al navegador.
    }

}

?>
