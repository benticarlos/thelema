<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#ventana">
        <span class="glyphicon glyphicon-plus-sign"></span>&nbsp;&nbsp;Insertar Misionero
    </button>
</div>
<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <center><h4><strong>Listado de <?php echo $total; ?> Misioneros</strong></h4></center>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <table class="table table-striped table-bordered table-hover table-responsive">
            <thead>
                <tr class="bg-info">
                    <th>#</th>
                    <th>Nombres</th>
                    <th>Apellido</th>
                    <th>Fecha Misión</th>
                    <th>Lumisial</th>
                    <th>Diocesis</th>
                    <th>Poblado</th>
                    <th>Operaciones</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $hoy = date('Y-n-j');
                foreach ($misioneros as $item):
                    $res = $hoy - $item->fecha_mision;
                    if ($res >= 3 && $res <= 5) {
                        ?>
                        <tr class="warning">
                        <?php } elseif ($res > 5) { ?>
                        <tr class="danger">
                        <?php } else { ?>
                        <tr>
                        <?php } ?>
                        <td><?php echo $i+=1; ?></td>
                        <td><?php echo $item->nombre; ?></td>
                        <td><?php echo $item->apellido; ?></td>
                        <td><?php echo $item->fecha_mision; ?></td>
                        <td><?php echo $item->lumisial; ?></td>
                        <td><?php echo $item->estado; ?></td>
                        <td><?php echo $item->poblado; ?></td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <?php echo form_open('misioneros/consulta'); ?>
                                        <input type="hidden" value="<?php echo $item->id; ?>" name="id_cons">
                                        <button type="submit" class="btn btn-info" data-toggle="tooltip">
                                            <span class="glyphicon glyphicon-search"></span></button>
                                        <?php echo form_close(); ?>
                                    </td>
                                    <td>
                                        <?php echo form_open('misioneros/consulta'); ?>
                                        <input type="hidden" value="<?php echo $item->id; ?>" name="id_cons">
                                        <input type="hidden" value="modificar" name="operacion">
                                        <button type="submit" class="btn btn-success" data-toggle="tooltip">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                        <?php echo form_close(); ?>
                                    </td>
                                    <td>
                                        <?php echo form_open('misioneros/consulta'); ?>
                                        <input type="hidden" value="<?php echo $item->id; ?>" name="id_cons">
                                        <input type="hidden" value="eliminar" name="operacion">
                                        <button type="submit" class="btn btn-danger" data-toggle="tooltip">
                                            <span class="glyphicon glyphicon-remove"></span>
                                        </button>
                                        <?php echo form_close(); ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php echo $this->pagination->create_links(); ?>
        <!--MODAL DE INSERTAR USUARIO-->
        <div class="modal fade" id="ventana" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Cerrar</span>
                        </button>
                        <h4 class="modal-title" id="ModalLabel"><strong>Insertar Datos de Usuario</strong></h4>
                    </div>
                    <?php echo form_open('misioneros/agregar'); ?>
                    <!-- INSERTAR DATOS DE USUARIO-->
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="input-group col-xs-12 col-sm-12">
                                <label for="Usuario"><strong>Nombre</strong></label>
                                <input type="text" id="formGroup" class="form-control" placeholder="Rita" name="nombre" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12 col-sm-12">
                                <label for="Apellido"><strong>Apellido</strong></label>
                                <input type="text" id="formGroup" class="form-control" placeholder="Gomez" name="apellido" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <label for="Cedula"><strong>Cédula</strong></label>
                                <input type="text" id="formGroup" class="form-control" placeholder="V-12345789" name="cedula" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <label for="telefono"><strong>Teléfono</strong></label>
                                <input type="text" id="formGroup" class="form-control" placeholder="04141231212" name="telefono" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12 col-sm-12">
                                <label for="email"><strong>Email</strong></label>
                                <input type="text" id="formGroup" class="form-control" placeholder="usuario@thelema.com" name="email" required autofocus>
                            </div>
                        </div>
                        <div class="input-group date">
                            <div class="input-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <label for="Fecha_nac"><strong>Fecha de Nacimiento</strong><em>(Año-Mes-Día)</em></label>
                                <input type="text" id="calendar" class="datepicker" placeholder="1989-01-19" name="fecha_nac" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12 col-sm-12">
                                <label for="Nacionalidad"><strong>Nacionalidad</strong></label>
                                <input type="text" id="formGroup" class="form-control" placeholder="Venezolana" name="nacionalidad" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12 col-sm-12">
                                <label for="Profesion"><strong>Profesión u Ocupación</strong></label>
                                <input type="text" id="formGroup" class="form-control" placeholder="Abogado" name="profesion" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12 col-sm-12">
                                <label for="Cursos"><strong>Cursos Realizados</strong></label>
                                <input type="text" id="formGroup" class="form-control" placeholder="ASOPROVIDA" name="cursos" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12 col-sm-12">
                                <label for="Edo_civil"><strong>Estado Civil</strong></label>
                                <input type="text" id="formGroup" class="form-control" placeholder="Casado" name="edo_civil" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12 col-sm-12">
                                <label for="Idiomas"><strong>Idiomas</strong></label>
                                <input type="text" id="formGroup" class="form-control" placeholder="Inglés" name="idiomas" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <label for="Fecha_nac"><strong>Fecha de Ingreso a la 2da Cámara</strong><em>(Año-Mes-Día)</em></label>
                                <input type="text" id="calendar" class="form-control" placeholder="2009-01-19" name="fecha_ingreso" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-12 col-sm-12">
                                <label for="Lugar_mision"><strong>Lugar de Misión Actual</strong></label>
                                <input type="text" id="formGroup" class="form-control" placeholder="El Valle" name="lugar_mision" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <label for="Fecha_mision"><strong>Fecha de Inicio de la actual Misión</strong><em>(Año-Mes-Día)</em></label>
                                <input type="text" id="calendar" class="form-control" placeholder="2014-01-19" name="fecha_mision" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                <label for="Ungido"><strong>¿Está Ungido?</strong>(Isis o Sacerdote)</label>
                                <select class="form-control" name="nivel">
                                    <option value="1" <?php echo set_select('ungido', '1'); ?>>Sí</option>
                                    <option value="2" <?php echo set_select('ungido', '2', TRUE); ?>>No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-xs-10 col-sm-10 col-md-12 col-lg-12">
                                <label for="lumisial"> <strong>Lumisial</strong></label>
                                <select class="form-control" name="lumisial" id="lumisial" style="width: 98%" autofocus required>
                                    <option value="0">Selecciona el Lumisial</option>
                                    <?php foreach ($lumisial as $item) : ?>
                                        <option value="<?php echo $item->id; ?>"><?php echo $item->lumisial . ' - ' . $item->estado . ' - ' . $item->poblado; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-saved"></span> &nbsp;Agregar</button>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script src="<?php echo base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
    <!--<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>-->
    <script src="<?php echo base_url(); ?>assets/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () { // inicio jquery
            $('#calendario').datepicker(); // iniciamos la funcion datepicker de jquery-ui
            $('#calendario').datepicker("option", "showAnim", "clip");
            // option se habilita
            // showAnim muestra una animacion: clip/slide /fold
            $('#calendario').datepicker("option", "dateFormat", "dd-mm-yy");
            // dateFormat formato de la fecha
            // ej: "d M, y"
        });
    </script>
</div><!-- ROW -->
