<div class="row">
    <h2><strong>Datos del Curso</strong></h2>
    <table class="table table-striped table-bordered table-hover table-responsive">
        <tr>
            <td><label>Curso</label></td>
            <td><?php echo $cursos[0]->curso; ?></td>
        </tr>
        <tr>
            <td><label>Descripcion</label></td>
            <td><?php echo $cursos[0]->descripcion; ?></td>
        </tr>
    </table>
    <table class="table">
        <tr>
            <td>
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#ventana">
                    <span class="glyphicon glyphicon-search"></span> Modificar Datos
                </button>
            </td>
        </tr>
    </table>
    <!--MODAL DE MODIFICAR CURSO -->
    <div class="modal fade" id="ventana" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="false">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Cerrar</span>
                    </button>
                    <h4 class="modal-title" id="ModalLabel"><strong>Modificar Datos deL Curso</strong></h4>
                </div>
                <?php echo form_open('cursos/modificar'); ?>
                <!-- MODIFICAR DATOS DE LA DIOCESIS-->
                <div class="modal-body">
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label for="curso"><strong>Nombre del Curso</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $cursos[0]->curso; ?>" name="curso" required autofocus>
                            <input type="hidden" value="<?php echo $cursos[0]->id; ?>" name="id_cons">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label for="poblado"><strong>Descripción del Curso</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $cursos[0]->descripcion; ?>" name="descripcion" autofocus>
                        </div>
                    </div>
                </div> <!-- FIN modal-body -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-saved">&nbsp;&nbsp;Modificar</span></button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
