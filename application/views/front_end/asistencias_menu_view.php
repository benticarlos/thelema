<div class="col-lg-12 col-md-12 col-sm-8 col-xs-12">
  <h4><strong>Asistencias</strong></h4>
</div>
<?php echo form_open('asistencias/mostrar'); ?>
  <div class="col-xs-12 col-sm-10 col-md-10 col-lg-6">
    <select class="form-control" name="evento" id="evento" style="width: 98%" autofocus required>
      <option value="0">Selecciona el Evento</option>
          <?php foreach ($eventos as $item) : ?>
            <option value="<?php echo $item->id; ?>"><?php echo $item->evento . ' - ' . $item->lugare; ?></option>
          <?php endforeach; ?>
    </select>
    </div>
  <div class="col-xs-12 col-sm-2 col-md-2 col-lg-4">
        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> &nbsp;buscar</span></button>
  </div>
<?php echo form_close(); ?>
<br /><br />
