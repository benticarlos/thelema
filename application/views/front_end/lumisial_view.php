    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#ventana">
            <span class="glyphicon glyphicon-plus-sign"></span>&nbsp;&nbsp;Insertar Lumisial
        </button>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <center><h4><strong>Listado de <?php echo $total; ?> Lumisiales</strong></h4></center>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <table class="table table-striped table-bordered table-hover table-responsive">
                <thead>
                    <tr class="bg-info">
                        <th>Nombre</th>
                        <th>Poblado</th>
                        <th>Diócesis</th>
                        <th>Dirección</th>
                        <th>Operación</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($lumisial as $item): ?>
                        <tr>
                            <td><?php echo $item->lumisial; ?></td>
                            <td><?php echo $item->poblado; ?></td>
                            <td><?php echo $item->estado; ?></td>
                            <td><?php echo $item->direccion; ?></td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <?php echo form_open('lumisial/consulta'); ?>
                                            <input type="hidden" value="<?php echo $item->id; ?>" name="id_cons">
                                            <button type="submit" class="btn btn-info btn-sm" data-toggle="tooltip">
                                                <span class="glyphicon glyphicon-search"></span></button>
                                            <?php echo form_close(); ?>
                                        </td>
                                        <td>
                                            <?php echo form_open('lumisial/consulta'); ?>
                                            <input type="hidden" value="<?php echo $item->id; ?>" name="id_cons">
                                            <input type="hidden" value="modificar" name="operacion">
                                            <button type="submit" class="btn btn-success btn-sm" data-toggle="tooltip">
                                                <span class="glyphicon glyphicon-edit"></span>
                                            </button>
                                            <?php echo form_close(); ?>
                                        </td>
                                        <td>
                                            <?php echo form_open('lumisial/consulta'); ?>
                                            <input type="hidden" value="<?php echo $item->id; ?>" name="id_cons">
                                            <input type="hidden" value="eliminar" name="operacion">
                                            <button type="submit" class="btn btn-danger btn-sm" data-toggle="tooltip">
                                                <span class="glyphicon glyphicon-remove"></span>
                                            </button>
                                            <?php echo form_close(); ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $this->pagination->create_links(); ?>
            <!--MODAL DE INSERTAR LUMISIAL-->
            <div class="modal fade" id="ventana" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Cerrar</span>
                            </button>
                            <h4 class="modal-title" id="ModalLabel"><strong>Insertar Datos del Lumisial</strong></h4>
                        </div>
                        <?php echo form_open('lumisial/agregar'); ?>
                        <!-- INSERTAR DATOS DEL LUMISIAL-->
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="input-group col-xs-12 col-sm-12">
                                    <label for="nombre"><strong>Nombre</strong></label>
                                    <input type="text" id="formGroup" class="form-control" placeholder="VM Laksmi Daimon" name="nombre" required autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <label for="Poblado"><strong>Poblado, Ciudad o Pueblo</strong></label>
                                    <input type="text" id="formGroup" class="form-control" placeholder="La Grita" name="poblado" autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <label for="Direccion"><strong>Dirección</strong></label>
                                    <input type="text" id="formGroup" class="form-control" placeholder="Av. Principal Casa S/N" name="direccion" autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                    <label for="diocesis"><strong>Diocesis</strong></label>
                                    <select class="form-control" name="diocesis" id="diocesis" style="width: 98%" autofocus required>
                                        <option value="0">Selecciona la Diocesis</option>
                                        <?php foreach ($diocesis as $item) : ?>
                                            <option value="<?php echo $item->id; ?>"><?php echo $item->estado; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-saved"></span> &nbsp;Agregar</button>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
