<div class="container">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#ventana">
            <span class="glyphicon glyphicon-plus-sign"></span>&nbsp;&nbsp;Insertar Hijo
        </button>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <center><h4><strong>Listado de Hijos Misioneros</strong></h4></center>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <table class="table table-striped table-bordered table-hover table-responsive">
                <thead>
                    <tr class="bg-info">
                        <th>Misionero</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Fecha de Nacimiento</th>
                        <th>Operación</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($hijos as $item): ?>
                        <tr>
                            <td><?php echo $item->nombre . ' ' . $item->apellido; ?></td>
                            <td><?php echo $item->nombreh; ?></td>
                            <td><?php echo $item->apellidoh; ?></td>
                            <td><?php echo $item->fecha_nach; ?></td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <?php echo form_open('hijos/consulta'); ?>
                                            <input type="hidden" value="<?php echo $item->id; ?>" name="id_cons">
                                            <button type="submit" class="btn btn-info btn-sm" data-toggle="tooltip">
                                                <span class="glyphicon glyphicon-search"></span></button>
                                            <?php echo form_close(); ?>
                                        </td>
                                        <td>
                                            <?php echo form_open('hijos/consulta'); ?>
                                            <input type="hidden" value="<?php echo $item->id; ?>" name="id_cons">
                                            <input type="hidden" value="modificar" name="operacion">
                                            <button type="submit" class="btn btn-success btn-sm" data-toggle="tooltip">
                                                <span class="glyphicon glyphicon-edit"></span>
                                            </button>
                                            <?php echo form_close(); ?>
                                        </td>
                                        <td>
                                            <?php echo form_open('hijos/consulta'); ?>
                                            <input type="hidden" value="<?php echo $item->id; ?>" name="id_cons">
                                            <input type="hidden" value="eliminar" name="operacion">
                                            <button type="submit" class="btn btn-danger btn-sm" data-toggle="tooltip">
                                                <span class="glyphicon glyphicon-remove"></span>
                                            </button>
                                            <?php echo form_close(); ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $this->pagination->create_links(); ?>
            <!--MODAL DE INSERTAR DATOS DEL HIJO-->
            <div class="modal fade" id="ventana" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Cerrar</span>
                            </button>
                            <h4 class="modal-title" id="ModalLabel"><strong>Insertar Datos del Hijo</strong></h4>
                        </div>
                        <?php echo form_open('hijos/agregar'); ?>
                        <!-- INSERTAR DATOS DEL Hijo-->
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="input-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                    <label for="diocesis"><strong>Padre o Madre Misionero</strong></label>
                                    <select class="form-control" name="misionero" id="misionero" style="width: 98%" autofocus required>
                                        <option value="0">Selecciona al Padre o la Madre</option>
                                        <?php foreach ($misionero as $item) : ?>
                                            <option value="<?php echo $item->id; ?>"><?php echo $item->nombre . ' ' . $item->apellido; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group col-xs-12 col-sm-12">
                                    <label for="nombre"><strong>Nombre</strong></label>
                                    <input type="text" id="formGroup" class="form-control" placeholder="José o María" name="nombre" required autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <label for="apellido"><strong>Apellido</strong></label>
                                    <input type="text" id="formGroup" class="form-control" placeholder="Pérez" name="apellido" required autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <label for="fecha_nac"><strong>Fecha de Nacimiento</strong><em>(Año-Mes-Día)</em></label>
                                    <input type="text" id="formGroup" class="form-control" placeholder="1989-01-19" name="fecha_nac" autofocus>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-saved"></span> &nbsp;Agregar</button>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
