<div class="row">
    <h2><strong>Datos del Lumisial</strong></h2>
    <table class="table table-striped table-bordered table-hover table-responsive">
        <tr>
            <td><label>Lumisial</label></td>
            <td><?php echo $lum[0]->lumisial; ?></td>
        </tr>
        <tr>
            <td><label>Poblado o Ciudad</label></td>
            <td><?php echo $lum[0]->poblado; ?></td>
        </tr>
        <tr>
            <td><label>Diocesis</label></td>
            <td><?php echo $lum[0]->estado; ?></td>
        </tr>
        <tr>
            <td><label>Dirección</label></td>
            <td><?php echo $lum[0]->direccion; ?></td>
        </tr>
    </table>
    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#ventana">
        <span class="glyphicon glyphicon-search"></span> Modificar Datos
    </button>
    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#cons_eliminar">
        <span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;Eliminar Usuario
    </button>
    <!--MODAL DE MODIFICAR LUMISIAL -->
    <div class="modal fade" id="ventana" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="false">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Cerrar</span>
                    </button>
                    <h4 class="modal-title" id="ModalLabel"><strong>Modificar Datos del Lumisial</strong></h4>
                </div>
                <?php echo form_open('lumisial/modificar'); ?>
                <!-- MODIFICAR DATOS DEL LUMISIAL-->
                <div class="modal-body">
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label for="lumisial"><strong>Lumisial</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $lum[0]->lumisial; ?>" name="lumisial" required autofocus>
                            <input type="hidden" value="<?php echo $lum[0]->id; ?>" name="id_cons">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label for="poblado"><strong>Poblado o Ciudad</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $lum[0]->poblado; ?>" name="poblado" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label for="direccion"><strong>Dirección</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $lum[0]->direccion; ?>" name="direccion" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label for="diocesis"><strong>Diocesis</strong></label>
                            <select class="form-control" name="diocesis" id="diocesis" style="width: 98%" autofocus required>
                                <option value="0">Selecciona la Diocesis</option>
                                <?php foreach ($diocesis as $item) : ?>
                                    <option value="<?php echo $item->id; ?>"><?php echo $item->estado; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div> <!-- FIN modal-body -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-saved">&nbsp;&nbsp;Modificar</span></button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
    <!--MODAL DE CONSULTA ELIMINAR -->
    <div class="modal fade" id="cons_eliminar" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="false">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Cerrar</span>
                    </button>
                    <h4 class="modal-title" id="ModalLabel"><strong>¿Desea eliminar el Lumisial?</strong></h4>
                </div>
                <div class="modal-body">
                    <table>
                        <tr>
                            <td>
                                &nbsp;&nbsp;
                            </td>
                            <td>
                                <?php echo form_open('lumisial/eliminar'); ?>
                                <input type="hidden" value="<?php echo $lumisial[0]->id; ?>" name="id_cons">
                                <button type="submit" class="btn btn-danger btn-lg" data-toggle="tooltip">
                                    <span class="glyphicon glyphicon-ok"></span> Sí
                                </button>
                                <?php echo form_close(); ?>
                            </td>
                            <td>
                                &nbsp;&nbsp;
                            </td>
                            <td>
                                <form>
                                    <button type="submit" class="btn btn-info btn-lg" data-dismiss="modal">
                                        <span class="glyphicon glyphicon-remove"></span> No
                                    </button>
                                </form>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
