<div class="row">
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <center><h4><strong>Datos del Usuario</strong></h4></center>
    </div>
    <table class="table table-striped table-bordered table-hover table-responsive">
        <tr>
            <td><label>Nombre y Apellido</label></td>
            <td><?php echo $usuario[0]->nombres; ?></td>
        </tr>
        <tr>
            <td><label>Cédula</label></td>
            <td><?php echo $usuario[0]->cedula; ?></td>
        </tr>
        <tr>
            <td><label>Teléfono</label></td>
            <td><?php echo $usuario[0]->telefono; ?></td>
        </tr>
        <tr>
            <td><label>Correo Electrónico</label></td>
            <td><?php echo $usuario[0]->email; ?></td>
        </tr>
        <tr>
            <td><label>Fecha de Registro</label></td>
            <td><?php echo $usuario[0]->fecha_reg; ?></td>
        </tr>
        <tr>
            <td><label>Nivel de Acceso</label></td>
            <td>
                <?php
                if ($usuario[0]->nivel == 0) {
                    echo 'Super Admin';
                } elseif ($usuario[0]->nivel == 1) {
                    echo 'Administrador';
                } elseif ($usuario[0]->nivel == 2) {
                    echo 'Supervisor';
                } elseif ($usuario[0]->nivel == 3) {
                    echo 'Normal';
                } elseif ($usuario[0]->nivel == 4) {
                    echo 'Consultor';
                } else {
                    echo '<strong>Por definir</strong>';
                }
                ?>
            </td>
        </tr>
    </table>
    <table class="table">
        <tr>
            <td>
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#ventana">
                    <span class="glyphicon glyphicon-search"></span> Modificar Datos
                </button>
            </td>
            <td>
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#cons_eliminar">
                    <span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;Eliminar Usuario
                </button>
            </td>

        </tr>
    </table>
    <!--MODAL DE INSERTAR USUARIO-->
    <div class="modal fade" id="ventana" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Cerrar</span>
                    </button>
                    <h4 class="modal-title" id="ModalLabel"><strong>Modificar Datos de Usuario</strong></h4>
                </div>
                <?php echo form_open('usuarios/modificar'); ?>
                <!-- INSERTAR DATOS DE USUARIO-->
                <div class="modal-body">
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12">
                            <label for="Usuario"><strong>Nombre y Apellido</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $usuario[0]->nombres; ?>" name="nombres" required autofocus>
                            <input type="hidden" value="<?php echo $usuario[0]->id; ?>" name="id_cons">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-10 col-sm-10 col-md-4 col-lg-4">
                            <label for="cedula"><strong>Cédula</strong></label>
                            <table>
                                <tr>
                                    <td>
                                        <select class="form-control" name="tipo_ced">
                                            <option value="V-"  <?php echo set_select('tipo_ced', 'V-', TRUE); ?>>V</option>
                                            <option value="J-"  <?php echo set_select('tipo_ced', 'J-'); ?>>J</option>
                                            <option value="E-"  <?php echo set_select('tipo_ced', 'E-'); ?>>E</option>
                                            <option value="G-"  <?php echo set_select('tipo_ced', 'G-'); ?>>G</option>
                                            <option value="M-"  <?php echo set_select('tipo_ced', 'M-'); ?>>M</option>
                                            <option value="P-"  <?php echo set_select('tipo_ced', 'P-'); ?>>P</option>
                                            <option value="R-"  <?php echo set_select('tipo_ced', 'R-'); ?>>R</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" id="formGroup" class="form-control" value="<?php echo $usuario[0]->cedula; ?>" name="cedula" required autofocus>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <label for="telefono"><strong>Teléfono</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $usuario[0]->telefono; ?>" name="telefono" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12">
                            <label for="email"><strong>Email</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $usuario[0]->email; ?>" required autofocus disabled>
                            <input type="hidden" value="<?php echo $usuario[0]->email; ?>" name="email">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-6 col-sm-6">
                            <label for="nivel"><strong>Nivel</strong></label>
                            <select class="form-control" name="nivel" disabled>
                                <?php if ($usuario[0]->nivel == 0): ?>
                                    <option value="0" <?php echo set_select('nivel', '0'); ?> selected>0 - Super Admin</option>
                                <?php endif; ?>
                                <?php if ($usuario[0]->nivel == 1): ?>
                                    <option value="1" <?php echo set_select('nivel', '1'); ?> selected>1 - Administrador</option>
                                <?php endif; ?>
                                <?php if ($usuario[0]->nivel == 2): ?>
                                    <option value="2" <?php echo set_select('nivel', '2'); ?> selected>2 - Supervisor</option>
                                <?php endif; ?>
                                <?php if ($usuario[0]->nivel == 3): ?>
                                    <option value="3" <?php echo set_select('nivel', '3'); ?> selected>3 - Normal</option>
                                <?php endif; ?>
                                <?php if ($usuario[0]->nivel == 4): ?>
                                    <option value="4" <?php echo set_select('nivel', '4'); ?> selected>4 - Consulta</option>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-saved">Modificar</span></button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
    <!--MODAL DE CONSULTA ELIMINAR -->
    <div class="modal fade" id="cons_eliminar" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="false">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Cerrar</span>
                    </button>
                    <h4 class="modal-title" id="ModalLabel"><strong>Desea eliminar el usuario?</strong></h4>
                </div>
                <div class="modal-body">
                    <table>
                        <tr>
                            <td>
                                &nbsp;&nbsp;
                            </td>
                            <td>
                                <?php echo form_open('usuarios/eliminar'); ?>
                                <input type="hidden" value="<?php echo $usuario[0]->id; ?>" name="id_cons">
                                <button type="submit" class="btn btn-danger btn-lg" data-toggle="tooltip">
                                    <span class="glyphicon glyphicon-ok"></span> Sí
                                </button>
                                <?php echo form_close(); ?>
                            </td>
                            <td>
                                &nbsp;&nbsp;
                            </td>
                            <td>
                                <form>
                                    <button type="submit" class="btn btn-info btn-lg" data-dismiss="modal">
                                        <span class="glyphicon glyphicon-remove"></span> No
                                    </button>
                                </form>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</table>
</div>
