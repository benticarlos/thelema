<div class="container">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
        <?php echo form_open('misioneros/consulta'); ?>
        <input type="hidden" name="id_cons" value="<?php echo $id_cons; ?>">
        <button type="input" class="btn btn-success btn-lg">
            <i class="fa fa-thumbs-up fa-2x"></i>&nbsp;&nbsp;Continuar
        </button>
        <?php echo form_close(); ?>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <center><h2><strong>Hijo Insertado con Éxito</strong></h2></center>
    </div>  
</div>
