<div class="row">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#ventana">
            <span class="glyphicon glyphicon-plus-sign"></span>&nbsp;&nbsp;Modificar Usuario
        </button>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <center><h4><strong>Configuración del Usuario</strong></h4></center>
    </div>
    <table class="table table-striped table-bordered table-hover table-responsive">
        <tr>
            <td><label>Nombre y Apellido</label></td>
            <td><?php echo $usuario[0]->nombres; ?></td>
        </tr>
        <tr>
            <td><label>Cédula</label></td>
            <td><?php echo $usuario[0]->cedula; ?></td>
        </tr>
        <tr>
            <td><label>Teléfono</label></td>
            <td><?php echo $usuario[0]->telefono; ?></td>
        </tr>
        <tr>
            <td><label>Correo Electrónico</label></td>
            <td><?php echo $usuario[0]->email; ?></td>
        </tr>
        <tr>
            <td><label>Fecha de Registro</label></td>
            <td><?php echo $usuario[0]->fecha_reg; ?></td>
        </tr>
        <tr>
            <td><label>Nivel de Acceso</label></td>
            <td>
                <?php
                if ($usuario[0]->nivel == 0) {
                    echo 'Super Admin';
                } elseif ($usuario[0]->nivel == 1) {
                    echo 'Administrador';
                } elseif ($usuario[0]->nivel == 2) {
                    echo 'Supervisor';
                } elseif ($usuario[0]->nivel == 3) {
                    echo 'Normal';
                } elseif ($usuario[0]->nivel == 4) {
                    echo 'Consultor';
                } else {
                    echo '<strong>Por definir</strong>';
                }
                ?>
            </td>
            </tbody>
    </table>
    <!--MODAL DE INSERTAR USUARIO-->
    <div class="modal fade" id="ventana" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Cerrar</span>
                    </button>
                    <h4 class="modal-title" id="ModalLabel"><strong>Modificar Datos de Usuario</strong></h4>
                </div>
                <?php echo form_open('usuarios/mod_config'); ?>
                <!-- INSERTAR DATOS DE USUARIO-->
                <div class="modal-body">
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12 col-md-10 col-lg-10">
                            <label for="Usuario"><strong>Nombre y Apellido</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $usuario[0]->nombres; ?>" name="nombres" required autofocus>
                            <input type="hidden" value="<?php echo $usuario[0]->id; ?>" name="id_cons">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-10 col-sm-10 col-md-4 col-lg-4">
                            <label for="cedula"><strong>Cédula</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $usuario[0]->cedula; ?>" name="cedula" required autofocus disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-10 col-md-6 col-lg-6">
                            <label for="clave"><strong>Clave</strong></label>
                            <input type="password" id="formGroup" class="form-control" name="clave" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-10 col-md-6 col-lg-6">
                            <label for="clave2"><strong>Confirme la clave</strong></label>
                            <input type="password" id="formGroup" class="form-control" name="clave2" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <label for="telefono"><strong>Teléfono</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $usuario[0]->telefono; ?>" name="telefono" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12 col-md-10 col-lg-10">
                            <label for="email"><strong>Email</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $usuario[0]->email; ?>" required autofocus disabled>
                            <input type="hidden" value="<?php echo $usuario[0]->email; ?>" name="email">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-6 col-sm-6">
                            <label for="nivel"><strong>Nivel</strong></label>
                            <select class="form-control" name="nivel" disabled>
                                <?php if ($usuario[0]->nivel == 1): ?>
                                    <option value="1" <?php echo set_select('nivel', '1'); ?> selected>1 - Administrador</option>
                                <?php endif; ?>
                                <?php if ($usuario[0]->nivel == 2): ?>
                                    <option value="2" <?php echo set_select('nivel', '2'); ?> selected>2 - Supervisor</option>
                                <?php endif; ?>
                                <?php if ($usuario[0]->nivel == 3): ?>
                                    <option value="3" <?php echo set_select('nivel', '3'); ?> selected>3 - Normal</option>
                                <?php endif; ?>
                                <?php if ($usuario[0]->nivel == 4): ?>
                                    <option value="4" <?php echo set_select('nivel', '4'); ?> selected>4 - Consulta</option>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-saved">&nbsp;Editar</span></button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</table>
</div>
