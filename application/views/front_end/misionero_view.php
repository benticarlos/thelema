<div class="row">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#hijo">
            <i class="fa fa-child"></i>&nbsp;&nbsp;Insertar Hijo
        </button>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
        <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#historial">
            <i class="fa fa-history"></i>&nbsp;&nbsp;Insertar Historial
        </button>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
        <center><h4><strong>Datos del Misionero</strong></h4></center>
    </div>
    <table class="table table-striped table-bordered table-hover table-responsive">
        <tr>
            <td><label>Código</label></td>
            <td><?php echo $misionero[0]->id; ?></td>
        </tr>
        <tr>
            <td><label>Nombre y Apellido</label></td>
            <td><?php echo $misionero[0]->nombre . ' ' . $misionero[0]->apellido; ?></td>
        </tr>
        <tr>
            <td><label>Cédula</label></td>
            <td><?php echo $misionero[0]->cedula; ?></td>
        </tr>
        <tr>
            <td><label>Teléfono</label></td>
            <td><?php echo $misionero[0]->telefono; ?></td>
        </tr>
        <tr>
            <td><label>Correo Electrónico</label></td>
            <td><?php echo $misionero[0]->email; ?></td>
        </tr>
        <tr>
            <td><label>Fecha de Nacimiento</label></td>
            <td><?php echo $misionero[0]->fecha_nac; ?></td>
        </tr>
        <tr>
            <td><label>Nacionalidad</label></td>
            <td><?php echo $misionero[0]->nacionalidad; ?></td>
        </tr>
        <tr>
            <td><label>Profesión u Ocupación</label></td>
            <td><?php echo $misionero[0]->profesion; ?></td>
        </tr>
        <tr>
            <td><label>Cursos Realizados</label></td>
            <td><?php echo $misionero[0]->cursos; ?></td>
        </tr>
        <tr>
            <td><label>Estado Civil</label></td>
            <td><?php echo $misionero[0]->edo_civil; ?></td>
        </tr>
        <tr>
            <td><label>Idiomas</label></td>
            <td><?php echo $misionero[0]->idiomas; ?></td>
        </tr
        <tr>
            <td><label>Fecha de Ingreso a la 2da Cámara</label></td>
            <td><?php echo $misionero[0]->fecha_ingreso; ?></td>
        </tr>
        <tr>
            <td><label>Lugar de Misión</label></td>
            <td><?php echo $misionero[0]->lugar_mision; ?></td>
        </tr>
        <tr>
            <td><label>Fecha de Inicio de la Actual Misión</label></td>
            <td><?php echo $misionero[0]->fecha_mision; ?></td>
        </tr>
        <tr>
            <td><label>¿Está Ungido?</label></td>
            <?php if ($misionero[0]->ungido == 1): ?>
                <td><?php echo "Sí"; ?></td>
            <?php else: ?>
                <td><?php echo "No"; ?></td>
            <?php endif; ?>
        </tr>
        <tr>
            <td><label>Lumisial</label></td>
            <td><?php echo $misionero[0]->lumisial; ?></td>
        </tr>
        <tr>
            <td><label>Diocesis</label></td>
            <td><?php echo $misionero[0]->estado; ?></td>
        </tr>
    </table>
    <!-- ----- Sección de Hijos  ----- -->
    <?php if ($hijos) : ?>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
            <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#hijo">
                <span class="glyphicon glyphicon-plus-sign"></span>&nbsp;&nbsp;Insertar más hijos
            </button>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <center><h4><strong>Hijos</strong></h4></center>
        </div>
        <table class="table table-striped table-bordered table-hover table-responsive">
            <thead>
                <tr class="bg-info">
                    <th>Nombre y Apellido</th>
                    <th>Fecha de Nacimiento</th>
                </tr>
            </thead>
            <?php foreach ($hijos as $item) : ?>
                <tr>
                    <td><?php echo $item->nombreh . ' ' . $item->apellidoh; ?></td>
                    <td><?php echo $item->fecha_nach; ?></td>
                <tr>
                <?php endforeach; ?>
        </table><br />
    <?php endif; ?>

    <!-- ----- Sección de Historial  ----- -->
    <?php if ($historial) : ?>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
            <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#historial">
                <i class="fa fa-history"></i> &nbsp;&nbsp;Insertar Historial
            </button>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <center><h4><strong>Historial</strong></h4></center>
        </div>
        <table class="table table-striped table-bordered table-hover table-responsive">
            <thead>
                <tr class="bg-warning">
                    <th>Lugar de la Antigua Misión</th>
                    <th>Fecha de Inicio</th>
                    <th>Fecha de Culminación</th>
                </tr>
            </thead>
            <?php foreach ($historial as $item) : ?>
                <tr>
                    <td><?php echo $item->lugar_misionh; ?></td>
                    <td><?php echo $item->fecha_inicio; ?></td>
                    <td><?php echo $item->fecha_fin; ?></td>
                <tr>
                <?php endforeach; ?>
        </table><br />
    <?php endif; ?>
    <!-- -------------- Botones ---------------- -->
    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
        <form>
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#ventana">
                <span class="glyphicon glyphicon-search"></span> Modificar Datos
            </button>
        </form>
    </div>
    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
        <form>
            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminar">
                <span class="glyphicon glyphicon-remove"></span> Eliminar Misionero
            </button>
        </form>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
        <form>
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#hijo">
                <i class="fa fa-child"></i>&nbsp;&nbsp;Insertar Hijo
            </button>
        </form>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
        <form>
            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#historial">
                <i class="fa fa-history"></i>&nbsp;&nbsp;Insertar Historial
            </button>
        </form>
    </div>
    <!-- ---- MODAL DE MODIFICAR MISIONERO ---- -->
    <div class="modal fade" id="ventana" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Cerrar</span>
                    </button>
                    <h4 class="modal-title" id="ModalLabel"><strong>Modificar Datos del Misionero</strong></h4>
                </div>
                <?php echo form_open('misioneros/modificar'); ?>
                <!-- MODIFICAR DATOS DEL MISIONERO -->
                <div class="modal-body">
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12">
                            <label for="nombre"><strong>Nombre</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $misionero[0]->nombre; ?>" name="nombre" required autofocus>
                            <input type="hidden" value="<?php echo $misionero[0]->id; ?>" name="id_cons">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12">
                            <label for="apellido"><strong>Apellido</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $misionero[0]->apellido; ?>" name="apellido" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <label for="cedula"><strong>Cédula</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $misionero[0]->cedula; ?>" name="cedula" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <label for="telefono"><strong>Teléfono</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $misionero[0]->telefono; ?>" name="telefono" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12">
                            <label for="email"><strong>Email</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $misionero[0]->email; ?>" required autofocus>
                            <input type="hidden" value="<?php echo $misionero[0]->email; ?>" name="email">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
                            <label for="Fecha_nac"><strong>Fecha de Nacimiento</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $misionero[0]->fecha_nac; ?>" name="fecha_nac" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12">
                            <label for="Nacionalidad"><strong>Nacionalidad</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $misionero[0]->nacionalidad; ?>" name="nacionalidad" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12">
                            <label for="Profesion"><strong>Profesión u Ocupación</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $misionero[0]->profesion; ?>" name="profesion" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12">
                            <label for="Cursos"><strong>Cursos Realizados</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $misionero[0]->cursos; ?>" name="cursos" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12">
                            <label for="Edo_civil"><strong>Estado Civil</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $misionero[0]->edo_civil; ?>" name="edo_civil" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12">
                            <label for="Idiomas"><strong>Idiomas</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $misionero[0]->idiomas; ?>" name="idiomas" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
                            <label for="Fecha_ingreso"><strong>Fecha de Ingreso a la 2da Cámara</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $misionero[0]->fecha_ingreso; ?>" name="fecha_ingreso" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12">
                            <label for="Lugar_mision"><strong>Lugar de Misión</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $misionero[0]->lugar_mision; ?>" name="lugar_mision" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
                            <label for="Fecha_mision"><strong>Fecha de Inicio de la Actual Misión</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $misionero[0]->fecha_mision; ?>" name="fecha_mision" required>
                            <input type="hidden" name="id_cons" value="<?php echo $misionero[0]->id; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-1 col-sm-1 col-md-1 col-lg-1">
                            <label for="ungido"><strong>¿Está Ungido?</strong></label>
                            <select class="form-control" name="ungido">
                                <option value="1" <?php echo set_select('ungido', '1'); ?>>Sí</option>
                                <option value="0" <?php echo set_select('ungido', '0'); ?>>No</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <label for="lumisial"> <strong>Lumisial</strong></label>
                            <select class="form-control" name="lumisial" id="lumisial" style="width: 98%" autofocus required>
                                <option value="0">Selecciona el Lumisial</option>
                                <?php foreach ($lumisial as $item) : ?>
                                    <option value="<?php echo $item->id; ?>"><?php echo $item->lumisial . ' - ' . $item->estado . ' - ' . $item->poblado; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-saved">&nbsp;Modificar</span></button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
    <!-- ---- MODAL Consulta eliminar ---- -->
    <div class="modal fade" id="eliminar" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Cerrar</span>
                    </button>
                    <h4 class="modal-title" id="ModalLabel"><strong>¿Desea eliminar los Datos del Misionero?</strong></h4>
                </div>
                <div class="modal-body">
                    <table>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <?php echo form_open('misioneros/eliminar'); ?>
                                <input type="hidden" value="<?php echo $misionero[0]->id; ?>" name="id_cons">
                                <button type="submit" class="btn btn-danger btn-lg">
                                    <span class="glyphicon glyphicon-ok"></span> Sí
                                </button>
                                <?php echo form_close(); ?>
                            </td>
                            <td>
                                <form>
                                    <button type="button" class="btn btn-info btn-lg" data-dismiss="modal">
                                        <span class="glyphicon glyphicon-remove"></span> No
                                    </button>
                                </form>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- ----- Boton insertar Hijo ---- -->
    <div class="modal fade" id="hijo" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Cerrar</span>
                    </button>
                    <h4 class="modal-title" id="ModalLabel"><strong>Insertar Datos del Hijo</strong></h4>
                </div>
                <?php echo form_open('hijos/misionero'); ?>
                <!-- INSERTAR DATOS DEL Hijo-->
                <div class="modal-body">
                    <div class="form-group">
                        <div class="input-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <label for="misionero"><strong>Padre o Madre Misionero</strong></label>
                            <select class="form-control" name="misionero" id="misionero" style="width: 98%" autofocus required>
                                <option value="0">Selecciona al Padre o la Madre</option>
                                <?php foreach ($misionero as $item) : ?>
                                    <option value="<?php echo $item->id; ?>"><?php echo $item->nombre . ' ' . $item->apellido; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12">
                            <label for="nombre"><strong>Nombre</strong></label>
                            <input type="text" id="formGroup" class="form-control" placeholder="José o María" name="nombre" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <label for="apellido"><strong>Apellido</strong></label>
                            <input type="text" id="formGroup" class="form-control" placeholder="Pérez" name="apellido" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <label for="fecha_nac"><strong>Fecha de Nacimiento</strong><em>(Año-Mes-Día)</em></label>
                            <input type="text" id="formGroup" class="form-control" placeholder="1989-01-19" name="fecha_nac" autofocus>
                            <input type="hidden" name="id_cons" value="<?php echo $item->id; ?>">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-saved"></span> &nbsp;Agregar</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
    <!-- ----- Boton insertar Historial ---- -->
    <div class="modal fade" id="historial" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Cerrar</span>
                    </button>
                    <h4 class="modal-title" id="ModalLabel"><strong>Insertar Historial</strong></h4>
                </div>
                <?php echo form_open('historial/misionero'); ?>
                <!-- INSERTAR DATOS DEL Hijo-->
                <div class="modal-body">
                  <div class="form-group">
                      <div class="input-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
                          <label for="diocesis"><strong>Misionero</strong></label>
                          <select class="form-control" name="misionero" id="misionero" style="width: 98%" autofocus required>
                            <option value="0">Selecciona el Misionero</option>
                                <?php foreach ($misionero as $item) : ?>
                                  <option value="<?php echo $item->id; ?>"><?php echo $item->nombre . ' ' . $item->apellido; ?></option>
                                <?php endforeach; ?>
                          </select>
                      </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12">
                            <label for="lugar_mision"><strong>Lugar de la Misión</strong></label>
                            <input type="text" id="formGroup" class="form-control" placeholder="Punto Fijo, Falcon - Keops" name="lugar_misionh" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <label for="fecha_inicio"><strong>Fecha de Inicio<em>(Año-Mes-Día)</em></strong></label>
                            <input type="text" id="formGroup" class="form-control" placeholder="2010-01-19" name="fecha_inicio" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <label for="fecha_fin"><strong>Fecha de Culminación</strong><em>(Año-Mes-Día)</em></label>
                            <input type="text" id="formGroup" class="form-control" placeholder="2013-12-31" name="fecha_fin" required autofocus>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-saved"></span> &nbsp;Agregar</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () { // inicio jquery
        $('#calendar').datepicker(); // iniciamos la funcion datepicker de jquery-ui
        $('#calendar').datepicker("option", "showAnim", "clip");
        // option se habilita
        // showAnim muestra una animacion: clip/slide /fold
        $('#calendar').datepicker("option", "dateFormat", "dd-mm-yy");
        // dateFormat formato de la fecha
        // ej: "d M, y"
    });
</script>
