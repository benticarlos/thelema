<div class="row">
    <h2><strong>Datos de la Diocesis</strong></h2>
    <table class="table table-striped table-bordered table-hover table-responsive">
        <tr>
            <td><label>Diocesis</label></td>
            <td><?php echo $dio[0]->estado; ?></td>
        </tr>
        <tr>
            <td><label>Encargado</label></td>
            <td><?php echo $dio[0]->encargado; ?></td>
        </tr>
    </table>
    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#ventana">
        <span class="glyphicon glyphicon-search"></span> Modificar Datos
    </button>
    <!--MODAL DE MODIFICAR DIOCESIS -->
    <div class="modal fade" id="ventana" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="false">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Cerrar</span>
                    </button>
                    <h4 class="modal-title" id="ModalLabel"><strong>Modificar Datos de la Diocesis</strong></h4>
                </div>
                <?php echo form_open('diocesis/modificar'); ?>
                <!-- MODIFICAR DATOS DE LA DIOCESIS-->
                <div class="modal-body">
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label for="lumisial"><strong>Diocesis</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $dio[0]->estado; ?>" name="estado" required autofocus>
                            <input type="hidden" value="<?php echo $dio[0]->id; ?>" name="id_cons">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label for="poblado"><strong>Encargado</strong></label>
                            <input type="text" id="formGroup" class="form-control" value="<?php echo $dio[0]->encargado; ?>" name="encargado" required autofocus>
                        </div>
                    </div>
                </div> <!-- FIN modal-body -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-saved">&nbsp;Modificar</span></button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
