<div class="row">
    <h2><strong>Datos de la Solicitud</strong></h2>
    <table class="table table-striped table-bordered table-hover table-responsive">
        <tr>
            <td><label>Asunto</label></td>
            <td><?php echo $solicitud[$i_cons - 1]->asunto; ?></td>
        </tr>
        <tr>
            <td><label>Contenido</label></td>
            <td><?php echo $solicitud[$i_cons - 1]->contenido; ?></td>
        </tr>
        <tr>
            <td><label>Fecha</label></td>
            <td><?php echo $solicitud[$i_cons - 1]->fecha; ?></td>
        </tr>
        <tr>
            <td><label>Institución</label></td>
            <td><?php echo $solicitud[$i_cons - 1]->institucion; ?></td>
        </tr> 
    </table>
    <table class="table">
        <tr>
            <td>
                <?php echo form_open('solicitud/mostrar'); ?>                        
                <button type="submit" class="btn btn-success" data-toggle="tooltip">
                    <i class='fa fa-backward'></i>&nbsp; Volver
                </button>                            
                <?php echo form_close(); ?> 
            </td>          
        </tr>
    </table>
</div>