<div class="row">
    <h3><strong>Administración de Solicitudes</strong></h3>
    <?php if ($nivel == 1 || $nivel == 0) { ?>
        <table>
            <tr>
                <td>
                    <!-- Imprimir  -->
                    <?php echo form_open('pdf_solicitud/reporte'); ?>
                    <button type="submit" class="btn btn-info btn-sm">
                        <i class="fa fa-file-pdf-o fa-2x"></i>&nbsp;&nbsp;Imprimir</button>
                    <?php echo form_close(); ?>
                </td>
            </tr>
        </table>
        <!-- 1ra Fila-->
        <table class="table table-striped table-bordered table-hover table-responsive">
            <thead>
                <tr class="bg-success">
                    <th>#</th>
                    <th>Código</th>
                    <th>Asunto</th>
                    <th>Fecha</th>
                    <th>Institución</th>
                    <th>Operaciones</th>
                </tr>
            </thead>
            <!-- n+1 Filas-->
            <tbody>
                <?php //public var $i=1; ?>
                <?php foreach ($solicitud as $item): ?>
                    <tr>
                        <td><?php echo $i+= 1; ?></td>
                        <td><?php echo $item->id; ?></td>
                        <td><?php echo $item->asunto; ?></td>
                        <td><?php echo $item->fecha; ?></td>
                        <td><?php echo $item->institucion; ?></td>

                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <?php echo form_open('solicitud/consulta'); ?>
                                        <input type="hidden" value="<?php echo $item->id; ?>" name="id_cons">
                                        <input type="hidden" value="<?php echo $i; ?>" name="i_cons">
                                        <button type="submit" class="btn btn-info" data-toggle="tooltip">
                                            <span class="glyphicon glyphicon-search"></span></button>
                                        <?php echo form_close(); ?>
                                    </td>
                                </tr>
                            </table>
                        </td>



                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php } else { ?>
        <div class="alert alert-error alert-danger alert-dismissible col-lg-6 col-md-8 col-sm-8 col-xs-10" role="alert">
            <a class="close" data-dismiss="alert" href="<?php echo base_url() . 'inicio'; ?>">×</a>
            <strong><center>Usuario sin permisos de edición</center></strong>
        </div>
    <?php } ?>
</div>
