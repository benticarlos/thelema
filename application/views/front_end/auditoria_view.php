<div class="row">
    <?php if ($nivel == 0) { ?>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
            <!-- Imprimir  -->
            <?php echo form_open('pdf_auditoria/reporte'); ?>
            <button type="submit" class="btn btn-primary btn-sm">
                <i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;Imprimir</button>
            <?php echo form_close(); ?>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <h4><strong>Auditoria - UGE</strong></h4>
        </div>
        <!-- 1ra Fila-->
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr class="bg-info">
                    <th>#</th>
                    <th>IP</th>
                    <th>Usuario</th>
                    <th>Acción</th>
                    <th>Fecha y Hora</th>
                    <th>Navegador</th>
                </tr>
            </thead>
            <!-- n+1 Filas-->
            <tbody>
                <?php foreach ($auditoria as $item): ?>
                    <tr>
                        <td><?php echo $item->id; ?></td>
                        <td><?php echo $item->ip; ?></td>
                        <td><?php echo $item->usuario; ?></td>
                        <td><?php echo $item->accion; ?></td>
                        <td><?php echo $item->tiempo_accion; ?></td>
                        <td><?php echo $item->navegador; ?></td>

                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php echo $this->pagination->create_links(); ?>
    <?php } else { ?>
        <div class="alert alert-error alert-danger alert-dismissible col-lg-6 col-md-8 col-sm-8 col-xs-10" role="alert">
            <a class="close" data-dismiss="alert" href="<?php echo base_url() . 'inicio'; ?>">×</a>
            <strong><center>Usuario sin permisos de edición</center></strong>
        </div>
    <?php } ?>
</div>
