<div class="row">
    <?php if ($nivel == 1 || $nivel == 0) { ?>
        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#ventana">
                <span class="glyphicon glyphicon-plus-sign"></span>&nbsp;&nbsp;Insertar Usuario
            </button>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
            <?php echo form_open('imprimir/usuarios'); ?>
            <button type="submit" class="btn btn-info btn-sm">
                <i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;Imprimir</button>
            <?php echo form_close(); ?>
        </div>
        <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
            <center><h4><strong>Administración de Usuarios</strong></h4></center>
        </div>
        <!-- 1ra Fila-->
        <table class="table table-striped table-bordered table-hover table-responsive">
            <thead>
                <tr class="bg-info">
                    <th>#</th>
                    <th>Nombres</th>
                    <th>Cedula</th>
                    <th>Teléfono</th>
                    <th>Email</th>
                    <th>Fecha de Registro</th>
                    <th>Nivel</th>
                    <th>Operaciones</th>
                </tr>
            </thead>
            <!-- n+1 Filas-->
            <tbody>
                <?php //public var $i=1; ?>
                <?php foreach ($usuarios as $item): ?>
                    <tr>
                        <td><?php echo $i+=1; ?></td>
                        <td><?php echo $item->nombres; ?></td>
                        <td><?php echo $item->cedula; ?></td>
                        <td><?php echo $item->telefono; ?></td>
                        <td><?php echo $item->email; ?></td>
                        <td><?php echo $item->fecha_reg; ?></td>
                        <?php if ($item->nivel == 0): ?>
                            <td><?php echo $item->nivel . " - Super Admin"; ?></td>
                        <?php endif; ?>
                        <?php if ($item->nivel == 1): ?>
                            <td><?php echo $item->nivel . " - Administrador"; ?></td>
                        <?php endif; ?>
                        <?php if ($item->nivel == 2): ?>
                            <td><?php echo $item->nivel . " - Supervisor"; ?></td>
                        <?php endif; ?>
                        <?php if ($item->nivel == 3): ?>
                            <td><?php echo $item->nivel . " - Normal"; ?></td>
                        <?php endif; ?>
                        <?php if ($item->nivel == 4): ?>
                            <td><?php echo $item->nivel . " - Consulta"; ?></td>
                        <?php endif; ?>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <?php echo form_open('usuarios/consulta_usuario'); ?>
                                        <input type="hidden" value="<?php echo $item->id; ?>" name="id_cons">
                                        <button type="submit" class="btn btn-info btn-sm" data-toggle="tooltip">
                                            <span class="glyphicon glyphicon-search"></span></button>
                                        <?php echo form_close(); ?>
                                    </td>
                                    <td>
                                        <?php echo form_open('usuarios/consulta_usuario'); ?>
                                        <input type="hidden" value="<?php echo $item->id; ?>" name="id_cons">
                                        <input type="hidden" value="modificar" name="operacion">
                                        <button type="submit" class="btn btn-success btn-sm" data-toggle="tooltip">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                        <?php echo form_close(); ?>
                                    </td>
                                    <td>
                                        <?php echo form_open('usuarios/consulta_usuario'); ?>
                                        <input type="hidden" value="<?php echo $item->id; ?>" name="id_cons">
                                        <input type="hidden" value="eliminar" name="operacion">
                                        <button type="submit" class="btn btn-danger btn-sm" data-toggle="tooltip">
                                            <span class="glyphicon glyphicon-remove"></span>
                                        </button>
                                        <?php echo form_close(); ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php echo $this->pagination->create_links(); ?>
    <?php } else { ?>
        <div class="alert alert-error alert-danger alert-dismissible col-lg-6 col-md-8 col-sm-8 col-xs-10" role="alert">
            <a class="close" data-dismiss="alert" href="<?php echo base_url() . 'inicio'; ?>">×</a>
            <strong><center>Usuario sin permisos de edición</center></strong>
        </div>
    <?php } ?>
    <!--MODAL DE INSERTAR USUARIO-->
    <div class="modal fade" id="ventana" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Cerrar</span>
                    </button>
                    <h4 class="modal-title" id="ModalLabel"><strong>Insertar Datos de Usuario</strong></h4>
                </div>
                <?php echo form_open('usuarios/agregar'); ?>
                <!-- INSERTAR DATOS DE USUARIO-->
                <div class="modal-body">
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12">
                            <label for="Usuario"><strong>Nombre y Apellido</strong></label>
                            <input type="text" id="formGroup" class="form-control" placeholder="Rita Gomez" name="nombres" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <label for="cedula"><strong>Cédula</strong></label>
                            <table>
                                <tr>
                                    <td>
                                        <select class="form-control" name="tipo_ced">
                                            <option value="V-"  <?php echo set_select('tipo_ced', 'V-', TRUE); ?>>V</option>
                                            <option value="J-"  <?php echo set_select('tipo_ced', 'J-'); ?>>J</option>
                                            <option value="E-"  <?php echo set_select('tipo_ced', 'E-'); ?>>E</option>
                                            <option value="G-"  <?php echo set_select('tipo_ced', 'G-'); ?>>G</option>
                                            <option value="M-"  <?php echo set_select('tipo_ced', 'M-'); ?>>M</option>
                                            <option value="P-"  <?php echo set_select('tipo_ced', 'P-'); ?>>P</option>
                                            <option value="R-"  <?php echo set_select('tipo_ced', 'R-'); ?>>R</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" id="formGroup" class="form-control" placeholder="12345789" name="cedula" required autofocus>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <label for="telefono"><strong>Teléfono</strong></label>
                            <input type="text" id="formGroup" class="form-control" placeholder="01231231212" name="telefono" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12">
                            <label for="email"><strong>Email</strong></label>
                            <input type="text" id="formGroup" class="form-control" placeholder="usuario@ucg.co" name="email" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12">
                            <label for="clave"><strong>Clave</strong></label>
                            <input type="password" id="formGroup" class="form-control" placeholder="Clave" name="password" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-12 col-sm-12">
                            <label for="claveconf"><strong>Confirmar Clave</strong></label>
                            <input type="password" id="formGroup" class="form-control" placeholder="Confirmar Clave" name="passwordconf" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-xs-6 col-sm-6">
                            <label for="nivel"><strong>Nivel</strong></label>
                            <select class="form-control" name="nivel">
                                <option value="1" <?php echo set_select('nivel', '1'); ?>>1 - Administrador</option>
                                <option value="2" <?php echo set_select('nivel', '2'); ?>>2 - Supervisor</option>
                                <option value="3" <?php echo set_select('nivel', '3', TRUE); ?>>3 - Normal</option>
                                <option value="4" <?php echo set_select('nivel', '4'); ?>>4 - Consulta</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-saved"></span> &nbsp;Agregar</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
