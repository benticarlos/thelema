<div class="container well" id="sha">
    <div class="row">
        <div class="col-xs-12">
            <img src="<?php echo base_url(); ?>assets/img/logo-acuario.jpg" class="img-responsive"><br />
        </div>
    </div>    
    <?php if (isset($error) && $error): ?><!--Mensaje si no valida el correo y la clave-->
        <div class="alert alert-error alert-danger alert-dismissible" role="alert">
            <a class="close" data-dismiss="alert" href="#">×</a>
            <strong>¡Correo y Clave no coinciden!</strong>
        </div>
    <?php endif; ?>
    <?php echo form_open('login/acceso'); ?>
    <div class="form-group">
        <div class="input-group col-xs-12 col-sm-12">
            <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
            <input type="text" id="formGroup" class="form-control" placeholder="usuario@server.com" name="email" required autofocus>
        </div>
    </div>
    <div class="form-group">
        <div class="input-group col-xs-12 col-sm-12">
            <span class="input-group-addon"><i class="fa fa-key"></i></span>
            <input type="password" id="formGroup" class="form-control" placeholder="Clave" name="password" required>
        </div>
    </div>  
    <button class="btn btn-sm btn-primary btn-block" type="submit" data-toggle="tooltip" data-placement="right" data-original-title="Iniciar Sesión">
        <span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;Iniciar Sesión
    </button>
    <div class="well-sm">
        <p style="text-align: center; font-size: 12px">
            <a href="<?php echo base_url(); ?>">Thelema</a> fue diseñado por  <a target="_blank" href="#"><strong>Carlos Benti</strong></a> 
            bajo  <a target="_blank" href="https://es.wikipedia.org/wiki/Licencia_MIT">Licencia MIT</a><br /><a href="https://facebook.com/benticarlos" target="blank"><i class="fa fa-facebook-square"></i>&nbsp;Facebook</a>
            &nbsp;<a href="https://twitter.com/benticarlos" target="blank"><i class="fa fa-twitter-square"></i>&nbsp;Twitter</a>
            &nbsp;<a href="https://instagram.com/benticarlos" target="blank"><i class="fa fa-instagram"></i>&nbsp;Instagram</a>
        </p>
    </div><!-- .container.footer-->
    <?php echo form_close(); ?>
</div> <!-- .container well-->
</div><!-- .container de header.php-->
<script src="<?php echo base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>	
</body>
</html>
