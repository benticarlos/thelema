<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$this->load->view('plantillas/front_end/header');
if (!($contenido == 'login_view')) {
    $this->load->view('plantillas/front_end/navbar');
}
$this->load->view('front_end/' . $contenido);
if (!($contenido == 'login_view')) {
    $this->load->view('plantillas/front_end/footer');
}

