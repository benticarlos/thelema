<!DOCTYPE hmtl>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title><?php echo "Thelema - " . $titulo; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/flatly-bootstrap.min.css">
        <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/cerulean-bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/lumen-bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/spacelab-bootstrap.min.css">-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css">
        <style>
            body {
                padding-top: 5px;
                padding-bottom: 10px;
            }
            #sha{
                max-width: 320px;
                  -webkit-box-shadow: 0px 0px 12px 0px rgba(48, 50, 50, 0.48);
                  -moz-box-shadow:    0px 0px 12px 0px rgba(48, 50, 50, 0.48);
                box-shadow:         0px 0px 5px 0px rgba(48, 50, 50, 0.48);
                border-radius: 3%;
            }
            #avatar{
                width: 370px;
                height: 370px;
                margin: 0px auto 10px;
                display: block;
                border-radius: 80%;
            }
        </style>
    </head>
    <body>
        <div class="container">
