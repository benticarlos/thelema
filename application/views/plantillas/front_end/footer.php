</div> <!-- .container well-->
<footer class="footer">
    <div class="container well">
        <p style="text-align: center; font-size: 12px">
            <a href="<?php echo base_url(); ?>">Thelema</a> fue diseñado por  <a target="_blank" href="#"><strong>Carlos Benti</strong></a> 
            bajo  <a target="_blank" href="https://es.wikipedia.org/wiki/Licencia_MIT">Licencia MIT</a><br /><a href="https://facebook.com/benticarlos" target="blank"><i class="fa fa-facebook-square"></i>&nbsp;Facebook</a>
            &nbsp;<a href="https://twitter.com/benticarlos" target="blank"><i class="fa fa-twitter-square"></i>&nbsp;Twitter</a>
            &nbsp;<a href="https://instagram.com/benticarlos" target="blank"><i class="fa fa-instagram"></i>&nbsp;Instagram</a>
        </p>
    </div><!-- .container.footer-->
</footer>
<!--<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>-->
<script src="<?php echo base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

</body>
</html>