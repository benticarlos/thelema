<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#thelema">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span><!--  icon-bar son iconos de la barra horizontal -->
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url() . 'inicio'; ?>"><small><strong>Thelema</strong></small></a>
        </div><!-- .navbar-header -->
        <div class="collapse navbar-collapse" id="thelema">
            <ul class="nav navbar-nav">
                <!--<li class="active"><?php echo anchor('inicio', 'Inicio'); ?></li>-->
                <?php if ($nivel == 1 || $nivel == 0): ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class='fa fa-street-view'></i>&nbsp;&nbsp;Misioneros<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><?php echo anchor("misioneros/mostrar", "&nbsp;<i class='fa fa-male'></i>&nbsp; &nbsp;&nbsp;Misioneros"); ?></li>
                            <li class="divider">
                            <li><?php echo anchor("hijos/mostrar", "&nbsp;<i class='fa fa-child'></i>&nbsp; &nbsp;&nbsp;Hijos Misioneros"); ?></li>
                            <li><?php echo anchor("historial/mostrar", "&nbsp;<i class='fa fa-history'></i>&nbsp; &nbsp;&nbsp;Historial Misioneros"); ?></li>
                            <li class="divider">
                            <li><?php echo anchor("cursos/mostrar", "<i class='fa fa-cubes'></i>&nbsp; &nbsp;Cursos"); ?></li>
                            <li><?php echo anchor("cursos_misioneros/mostrar", "<i class='fa fa-graduation-cap'></i>&nbsp; &nbsp;Cursos Misioneros"); ?></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class='fa fa-sitemap'></i>&nbsp;&nbsp;Estructura<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><?php echo anchor("lumisial/mostrar", "<i class='fa fa-university'></i>&nbsp &nbspLumisiales"); ?></li>
                            <li><?php echo anchor("diocesis/mostrar", "<i class='fa fa-globe'></i>&nbsp; &nbsp;Diocesis"); ?></li>
                            <li class="divider">
                            <li><?php echo anchor("eventos/mostrar", "<i class='fa fa-expand'></i>&nbsp; &nbsp;Eventos"); ?></li>
                            <li><?php echo anchor("asistencias/mostrar", "<i class='fa fa-hand-o-up'></i>&nbsp; &nbsp;Asistencias"); ?></li>
                        </ul>
                    </li>
                <?php endif; ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if ($nivel == 0): ?>
                    <li><?php echo anchor("auditoria", "<i class='fa fa-list-alt'></i>&nbsp &nbspAuditoría"); ?></li>
                <?php endif; ?>
                <?php if ($nivel == 1 || $nivel == 0): ?>
                    <li><?php echo anchor("usuarios", "<i class='fa fa-users'></i> &nbsp&nbspUsuarios"); ?></li>
                <?php endif; ?>
                <li class="divider"></li>
                <li>
                    <?php echo form_open('usuarios/config'); ?>
                    <input type="hidden" value="<?php echo $id; ?>" name="id_cons">
                    <button type="submit" style="background: none; border: none; vertical-align: central; padding-top: 19px">
                        <i class='fa fa-cogs'>&nbsp;</i><?php echo $nombres ?>
                    </button>
                    <?php echo form_close(); ?>
                </li>
                <li><?php echo anchor("inicio/logout", "<i class='fa fa-sign-out'></i>&nbsp &nbspSalir"); ?></li>
            </ul>
        </div><!-- .collapse navbar-collapse -->
    </div><!-- .container-fluid -->
</nav>
