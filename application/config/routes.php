<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
//$route['default_controller'] = "login/acceso";
//$route['default_controller'] = "welcome";
$route['default_controller'] = "login";
$route['404_override'] = '';

///// ROUTES FOR PAGINATIONS
// Pagination for Auditoria
$route['auditoria/(:num)'] = 'auditoria/mostrar';
$route['auditoria/'] = 'auditoria/mostrar';
// Pagination for Misioneros
$route['misioneros/(:num)'] = 'misioneros/mostrar';
$route['misioneros/'] = 'misioneros/mostrar';
// Pagination for Usuarios
$route['usuarios/(:num)'] = 'usuarios/consultar';
$route['usuarios/'] = 'usuarios/consultar';
// Pagination for Lumisial
$route['lumisial/(:num)'] = 'lumisial/mostrar';
$route['lumisial/'] = 'lumisial/mostrar';
// Pagination for Diocesis
$route['diocesis/(:num)'] = 'diocesis/mostrar';
$route['diocesis/'] = 'diocesis/mostrar';
// Pagination for Cursos
$route['cursos/(:num)'] = 'cursos/mostrar';
$route['cursos/'] = 'cursos/mostrar';

// Pagination for Cursos Misioneros
$route['cursos_misioneros/(:num)'] = 'cursos_misioneros/mostrar';
$route['cursos_misioneros/'] = 'cursos_misioneros/mostrar';

// Pagination for Hijos
$route['hijos/(:num)'] = 'hijos/mostrar';
$route['hijos/'] = 'hijos/mostrar';

// Pagination for Historial
$route['historial/(:num)'] = 'historial/mostrar';
$route['historial/'] = 'historial/mostrar';

// Pagination for Eventos
$route['eventos/(:num)'] = 'eventos/mostrar';
$route['eventos/'] = 'eventos/mostrar';

// Pagination for Asistencias
$route['asistencias/(:num)'] = 'asistencias/mostrar';
$route['asistencias/'] = 'asistencias/mostrar';

/* End of file routes.php */
/* Location: ./application/config/routes.php */
