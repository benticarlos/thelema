<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auditoria_model extends CI_Model {

    function total_auditoria() {
        return $this->db->count_all('sesion');
    }

    function consultar($seg, $porpag) {
        $this->db->select('*')
                ->from('sesion')
                ->limit($seg, $porpag)
                ->order_by('id', 'desc');
        $cons = $this->db->get();
        return $cons->result();
    }

}
