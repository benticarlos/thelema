<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Solicitud_model extends CI_Model {

    function consultar_todos() {
//        $sql = "SELECT * FROM sesion LIMIT 25"; 
//        $cons = $this->db->query($sql);
        //$this->db->select('*')->from('solicitud')->limit(30)->order_by('id', 'desc');
        $this->db->select('s.asunto, s.fecha, u.institucion, s.id')->from('solicitud s')->join('usuario u', 'u.id = s.id_usuario', 'left');
        $cons = $this->db->get();
        return $cons->result();
    }

    function consulta($id_consu) {
//        SELECT c.fecha, c.consumo, u.institucion
//        FROM consumo c
//        LEFT JOIN usuario u ON u.id = c.id_usuario
//        WHERE c.id = '4'
//        LIMIT 0, 30
        $this->db->select('s.asunto, s.contenido, s.fecha, u.institucion, s.id')->from('solicitud s')->join('usuario u', 'u.id = s.id_usuario', 'left');
//        $this->db->where('c.id = u.id');
        $cons = $this->db->get();
        return $cons->result();
    }

}
