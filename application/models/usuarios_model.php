<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Usuarios_model extends CI_Model {

    var $details;

    function validate_user($email, $password) {
    // Build a query to retrieve the user's details
    // based on the received username and password
        $this->db->from('usuario');
        $this->db->where('email', $email);
        $this->db->where('password', sha1($password));
        $login = $this->db->get()->result();

        // The results of the query are stored in $login.
        // If a value exists, then the user account exists and is validated
        if (is_array($login) && count($login) == 1) {
        // Set the users details into the $details property of this class
            $this->details = $login[0];
            // Call set_session to set the user's session vars via CodeIgniter
            $this->set_session();
            return true;
        }

        return false;
    }

    function total_usuarios() {
        return $this->db->count_all('usuario');
    }

    function set_session() {
    // session->set_userdata is a CodeIgniter function that
    // stores data in CodeIgniter's session storage.  Some of the values are built in
    // to CodeIgniter, others are added.  See CodeIgniter's documentation for details.
        $this->session->set_userdata(array(
            'id' => $this->details->id,
            'email' => $this->details->email,
            'nombres' => $this->details->nombres,
            'cedula' => $this->details->cedula,
            'nivel' => $this->details->nivel,
            'id_usuario' => $this->details->id_usuario,
            'isLoggedIn' => true
                )
        );
    }

    function consultar($seg, $porpag) {
        $this->db->select('*')
                ->from('usuario')
                ->limit($seg, $porpag)
                ->order_by('nombres', 'asc');
        $cons = $this->db->get();
        return $cons->result();
    }

    function consulta_usuario($id_cons) {
        $this->db->select('*');
        $this->db->from('usuario');
        $this->db->where('id = ' . $id_cons);
        $cons = $this->db->get();
        return $cons->result();
    }

    function cons_id($nomb_user) {
        $this->db->select('id');
        $this->db->from('usuario');
        $this->db->where('nombres =' . $nomb_user);
        $cons = $this->db->get();
        return $cons->result();
    }

}
