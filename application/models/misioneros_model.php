<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Misioneros_model extends CI_Model {

    ////////// Misioneros
    function total_misioneros() {
        return $this->db->count_all('misionero');
    }

    function misionero_menu() {
        $this->db->select('m.id, m.nombre, m.apellido, m.fecha_mision, l.lumisial, l.poblado, d.estado')
                ->from('misionero m')
                ->join('lumisial l', 'l.id = m.lumisial_id', 'left')
                ->join('diocesis d', 'd.id = l.diocesis_id', 'left')
                ->order_by('m.nombre', 'asc');
        $cons = $this->db->get();
        return $cons->result();
    }

    function consultar($seg, $porpag) {
        $this->db->select('m.id, m.nombre, m.apellido, m.fecha_mision, l.lumisial, l.poblado, d.estado')
                ->from('misionero m')
                ->join('lumisial l', 'l.id = m.lumisial_id', 'left')
                ->join('diocesis d', 'd.id = l.diocesis_id', 'left')
                ->limit($seg, $porpag)
                ->order_by('m.nombre', 'asc');
        //->order_by('m.id', 'asc');
        $cons = $this->db->get();
        return $cons->result();
    }

    function consulta($id_cons) {
        $this->db->select('m.id, m.nombre, m.apellido, m.cedula, m.telefono, m.email, m.fecha_nac, m.nacionalidad, m.profesion, m.cursos, m.edo_civil, m.idiomas, m.fecha_ingreso, m.lugar_mision, m.fecha_mision, m.ungido, l.lumisial, l.poblado, d.estado')
                ->from('misionero m')
                ->join('lumisial l', 'l.id = m.lumisial_id', 'left')
                ->join('diocesis d', 'd.id = l.diocesis_id', 'left')
                ->where('m.id = ' . $id_cons);
        $cons = $this->db->get();
        return $cons->result();
    }

////////////////////// Hijos
    function total_hijos() {
        return $this->db->count_all('hijos');
    }

    function hijos($seg, $porpag) {
        $this->db->select('h.id, h.nombreh, h.apellidoh, h.fecha_nach, m.nombre, m.apellido')
                ->from('hijos h')
                ->join('misionero m', 'm.id = h.misionero_id', 'left')
                ->limit($seg, $porpag)
                ->order_by('m.nombre', 'asc');
        $cons = $this->db->get();
        return $cons->result();
    }

    function hijos_menu() {
        $this->db->select('h.id, h.nombreh, h.apellidoh, h.fecha_nach, m.nombre, m.apellido')
                ->from('hijos h')
                ->join('misionero m', 'm.id = h.misionero_id', 'left')
                ->order_by('m.nombre', 'asc');
        $cons = $this->db->get();
        return $cons->result();
    }

    function hijos_cons($id_cons) {
        $this->db->select('h.nombreh, h.apellidoh, h.fecha_nach, m.id')
                ->from('hijos h')
                ->join('misionero m', 'm.id = h.misionero_id', 'left')
                ->where('m.id = ' . $id_cons)
                ->order_by('h.fecha_nach');
        $cons = $this->db->get();
        return $cons->result();
    }

    ////////////////////// Historial
    function total_historial() {
        return $this->db->count_all('historial');
    }

    function historial($seg, $porpag) {
        $this->db->select('h.id, h.lugar_misionh, h.fecha_inicio, h.fecha_fin, m.nombre, m.apellido')
                ->from('historial h')
                ->join('misionero m', 'm.id = h.misionero_id', 'left')
                ->limit($seg, $porpag)
                ->order_by('m.nombre', 'asc');
        $cons = $this->db->get();
        return $cons->result();
    }

    function historial_menu() {
        $this->db->select('h.id, h.lugar_misionh, h.fecha_inicio, h.fecha_fin, m.nombre, m.apellido')
                ->from('historial h')
                ->join('misionero m', 'm.id = h.misionero_id', 'left')
                ->order_by('lumisial', 'asc');
        $cons = $this->db->get();
        return $cons->result();
    }

    function historial_cons($id_cons) {
        $this->db->select('h.id, h.lugar_misionh, h.fecha_inicio, h.fecha_fin, m.nombre, m.apellido')
                ->from('historial h')
                ->join('misionero m', 'm.id = h.misionero_id', 'left')
                ->where('m.id = ' . $id_cons)
                ->limit('1')
                ->order_by('h.fecha_inicio', 'desc');
        $cons = $this->db->get();
        return $cons->result();
    }

    //////////////////// Lumisial
    function total_lumisial() {
        return $this->db->count_all('lumisial');
    }

    function lumisial($seg, $porpag) {
        $this->db->select('l.id, l.lumisial, l.poblado, l.direccion, d.estado')
                ->from('lumisial l')
                ->join('diocesis d', 'd.id = l.diocesis_id', 'left')
                ->limit($seg, $porpag)
                ->order_by('lumisial', 'asc');
        $cons = $this->db->get();
        return $cons->result();
    }

    function lumisial_menu() {
        $this->db->select('l.id, l.lumisial, l.poblado, l.direccion, d.estado')
                ->from('lumisial l')
                ->join('diocesis d', 'd.id = l.diocesis_id', 'left')
                ->order_by('lumisial', 'asc');
        $cons = $this->db->get();
        return $cons->result();
    }

    function lumisial_cons($id_cons) {
        $this->db->select('l.id, l.lumisial, l.poblado, l.direccion, d.estado')
                ->from('lumisial l')
                ->join('diocesis d', 'd.id = l.diocesis_id', 'left')
                ->where('l.id = ' . $id_cons)
                ->limit('1')
                ->order_by('l.lumisial');
        $cons = $this->db->get();
        return $cons->result();
    }

    //////////// Diocesis
    function total_diocesis() {
        return $this->db->count_all('diocesis');
    }

    function diocesis($seg, $porpag) {
        $this->db->select('id, estado, encargado')
                ->from('diocesis')
                ->limit($seg, $porpag)
                ->order_by('estado', 'asc');
        $cons = $this->db->get();
        return $cons->result();
    }

    function diocesis_menu() {
        $this->db->select('id, estado, encargado')
                ->from('diocesis')
                ->order_by('estado', 'asc');
        $cons = $this->db->get();
        return $cons->result();
    }

    function diocesis_cons($id_cons) {
        $this->db->select('id, estado, encargado')
                ->from('diocesis')
                ->where('id = ' . $id_cons)
                ->limit('1')
                ->order_by('estado');
        $cons = $this->db->get();
        return $cons->result();
    }

    /////////// Cursos
    function total_cursos() {
        return $this->db->count_all('cursos');
    }

    function total_cursos_misioneros() {
        return $this->db->count_all('cursos_misioneros');
    }

    function cursos_misioneros($seg, $porpag) {
        $this->db->select('m.nombre, m.apellido, c.id, c.curso')
                ->from('misionero m')
                ->join('cursos_misioneros cm', 'm.id = cm.misioneros_id', 'inner')
                ->join('cursos c', 'cm.cursos_id = c.id', 'inner')
                ->limit($seg, $porpag)
                ->order_by('c.curso');
        $cons = $this->db->get();
        return $cons->result();
    }

    function cursos($seg, $porpag) {
        $this->db->select('id, curso, descripcion')
                ->from('cursos')
                ->order_by('curso', 'asc')
                ->limit($seg, $porpag);
        $cons = $this->db->get();
        return $cons->result();
    }

    function cursos_menu() {
        $this->db->select('id, curso, descripcion')
                ->from('cursos')
                ->order_by('curso', 'asc');
        $cons = $this->db->get();
        return $cons->result();
    }

    function cursos_misioneros_menu() {
        $this->db->select('m.nombre, m.apellido, c.curso')
                ->from('misionero m')
                ->join('cursos_misioneros cm', 'm.id = cm.misioneros_id', 'inner')
                ->join('cursos c', 'cm.cursos_id = c.id', 'inner')
                ->limit($seg, $porpag);
        $cons = $this->db->get();
        return $cons->result();
    }

    function cursos_cons($id_cons) {
        $this->db->select('id, curso, descripcion')
                ->from('cursos')
                ->where('id = ' . $id_cons)
                ->order_by('curso');
        $cons = $this->db->get();
        return $cons->result();
    }

/////////// Eventos
    function total_eventos() {
        return $this->db->count_all('eventos');
    }

    function total_eventos_misioneros() {
        return $this->db->count_all('cursos_misioneros');
    }

    function asistencias($seg, $porpag) {
        $this->db->select('m.id, m.nombre, m.apellido, e.id, e.evento')
                ->from('misionero m')
                ->join('eventos_misioneros em', 'm.id = em.misioneros_id', 'inner')
                ->join('eventos e', 'em.eventos_id = e.id', 'inner')
                //->where('m.id = ' . $id_cons)
                //->where('m.id = 1')
                ->limit($seg, $porpag)
                ->order_by('m.nombre');
        $cons = $this->db->get();
        return $cons->result();
    }

    function eventos($seg, $porpag) {
        $this->db->select('id, evento, fecha_inicioe, fecha_fine, lugare')
                ->from('eventos')
                ->order_by('evento')
                ->limit($seg, $porpag);
        $cons = $this->db->get();
        return $cons->result();
    }

    function eventos_menu() {
        $this->db->select('id, evento, lugare')
                ->from('eventos')
                ->order_by('evento');
        $cons = $this->db->get();
        return $cons->result();
    }

    function eventos_misioneros_menu() {
        $this->db->select('m.nombre, m.apellido, c.curso')
                ->from('misionero m')
                ->join('cursos_misioneros cm', 'm.id = cm.misioneros_id', 'inner')
                ->join('cursos c', 'cm.cursos_id = c.id', 'inner')
                ->limit($seg, $porpag);
        $cons = $this->db->get();
        return $cons->result();
    }

    function eventos_cons($id_cons) {
        $this->db->select('id, evento, fecha_inicioe, fecha_fine, lugare')
                ->from('eventos')
                ->where('id = ' . $id_cons)
                ->order_by('evento');
        $cons = $this->db->get();
        return $cons->result();
    }

}
