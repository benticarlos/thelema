<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Usuarios extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('isLoggedIn')) {
            redirect('/login/show_login');
        }
        $this->load->model('Usuarios_model');
        $this->load->library('pagination');
    }

    function index() {
        if ($this->session->userdata('isLoggedIn')) {
            $user_id = $this->session->userdata('id');
            redirect('usuarios/consultar');
        } else {
            $this->show_login(false);
        }
    }

    function consultar() {
        /* URL a la que se desea agregar la paginación */
        $config['base_url'] = base_url() . '/usuarios/consultar/';

        /* Obtiene el total de registros a paginar */
        $config['total_rows'] = $this->Usuarios_model->total_usuarios();

        /* Obtiene el numero de registros a mostrar por pagina */
        $page = 12;
        $config['per_page'] = $page;

        /* Indica que segmento de la URL tiene la paginación, por default es 3 */
        $config['uri_segment'] = '10';
        $config['num_links'] = $config['total_rows'] / $page;
        /* Se personaliza la paginación para que se adapte a bootstrap */
        $config['full_tag_open'] = '<ul class="pagination pagination-lg">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><span>';
        $config['cur_tag_close'] = '<span></span></span></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
//        $config['first_link'] = '«';
        $config['first_link'] = 'Primera';
//        $config['prev_link'] = '‹';
        $config['prev_link'] = 'Anterior';
        $config['last_link'] = 'Última';
//        $config['last_link'] = '»';
//        $config['next_link'] = '›';
        $config['next_link'] = 'Siguiente';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        /* Se inicializa la paginacion */
        $this->pagination->initialize($config);
        ////////////////////////////////////////////////////////////
        $datos['usuarios'] = $this->Usuarios_model->consultar($config['per_page'], $this->uri->segment(3));
///////////////////////////// VISTA
        $datos['i'] = 0;
        $datos['nombres'] = $this->session->userdata('nombres');
        $datos['id'] = $this->session->userdata('id');
        $datos['nivel'] = $this->session->userdata('nivel');
        $datos['titulo'] = 'Usuarios';
        $datos['contenido'] = 'usuarios_view';
        $this->load->view('plantillas/plantilla', $datos);
    }

    function config() {
//        $data = array();
        $id_cons = $_POST['id_cons'];
        $datos['usuario'] = $this->Usuarios_model->consulta_usuario($id_cons);
///////////////////////////// VISTA
        $datos['nombres'] = $this->session->userdata('nombres');
        $datos['id'] = $this->session->userdata('id');
        $datos['nivel'] = $this->session->userdata('nivel');
        $datos['titulo'] = 'Usuarios';
        $datos['contenido'] = 'config_view';
        $this->load->view('plantillas/plantilla', $datos);
////////////////////////// Auditoria
        $arrayCamp = array(
            'ip' => $this->session->userdata('ip_address'),
            'usuario' => $this->session->userdata('email'),
            'navegador' => $this->session->userdata('user_agent'),
            'accion' => "CONSULTA Usuario",
            'tiempo_accion' => date("Y-n-j H:i:s")
        );
        $this->db->insert('sesion', $arrayCamp);
    }

    function consulta_usuario() {
//        $data = array();
        $id_cons = $_POST['id_cons'];
        $datos['usuario'] = $this->Usuarios_model->consulta_usuario($id_cons);
///////////////////////////// VISTA
        $datos['nombres'] = $this->session->userdata('nombres');
        $datos['id'] = $this->session->userdata('id');
        $datos['nivel'] = $this->session->userdata('nivel');
        $datos['titulo'] = 'Usuarios';
        $datos['contenido'] = 'consulta_view';
        $this->load->view('plantillas/plantilla', $datos);
////////////////////////// Auditoria
        $arrayCamp = array(
            'ip' => $this->session->userdata('ip_address'),
            'usuario' => $this->session->userdata('email'),
            'navegador' => $this->session->userdata('user_agent'),
            'accion' => "CONSULTA Usuario",
            'tiempo_accion' => date("Y-n-j H:i:s")
        );
        $this->db->insert('sesion', $arrayCamp);
    }

    function Agregar() {
        $data = array();
        $tipo_ced = $_POST['tipo_ced'];
//////////////////////////////////////////////
        $this->form_validation->set_rules('nombres', 'nombres', 'required|trim|xss_clean|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('cedula', 'cedula', 'required|trim|xss_clean|min_length[6]|max_length[15]');
        $this->form_validation->set_rules('telefono', 'telefono', 'required|trim|xss_clean|min_length[7]|max_length[20]');
        $this->form_validation->set_rules('email', 'email', 'required|trim|xss_clean|valid_email|max_length[20]');
        $this->form_validation->set_rules('password', 'password', 'required|trim|xss_clean|min_length[4]|max_length[20]');
        $this->form_validation->set_rules('passwordconf', 'passwordconf', 'required|trim|xss_clean|min_length[4]|max_length[20]');
        $this->form_validation->set_rules('nivel', 'nivel', 'required|trim|xss_clean|min_length[4]|max_length[20]');
//////////// Mensaje de Validación
        $this->form_validation->set_message('valid_email', '<b>El campo %s debe ser valido para Email</b>');
        $this->form_validation->set_message('required', '<b>El campo %s es requerido</b>');
//////////// Seguridad XSS
        $resp_nombres = $this->security->xss_clean($this->input->post('nombres'));
        $resp_cedula = $this->security->xss_clean($this->input->post('cedula'));
        $resp_telefono = $this->security->xss_clean($this->input->post('telefono'));
        $resp_email = $this->security->xss_clean($this->input->post('email'));
        $resp_password = $this->security->xss_clean($this->input->post('password'));
        $resp_passwordconf = $this->security->xss_clean($this->input->post('passwordconf'));
        $resp_nivel = $this->security->xss_clean($this->input->post('nivel'));
////////////
        if ($resp_password === $resp_passwordconf) {
            $arrayCampos = array(
                'email' => $resp_email,
                'nombres' => $resp_nombres,
                'password' => sha1($resp_password),
                'nombres' => $resp_nombres,
                'cedula' => $tipo_ced . $resp_cedula,
                'telefono' => $resp_telefono,
                'fecha_reg' => date("Y-n-j H:i:s"),
                'nivel' => $resp_nivel
            );
            $this->db->insert('usuario', $arrayCampos);
////////////////////////// Auditoria
            $arrayCamp = array(
                'ip' => $this->session->userdata('ip_address'),
                'usuario' => $this->session->userdata('email'),
                'navegador' => $this->session->userdata('user_agent'),
                'accion' => "INSERT Usuario",
                'tiempo_accion' => date("Y-n-j H:i:s")
            );
            $this->db->insert('sesion', $arrayCamp);
            ////////////////////////
            redirect('usuarios');
        } else {
////////////////////////// Auditoria
            $arrayCamp = array(
                'ip' => $this->session->userdata('ip_address'),
                'usuario' => $this->session->userdata('email'),
                'navegador' => $this->session->userdata('user_agent'),
                'accion' => "INSERT FALLIDO",
                'tiempo_accion' => date("Y-n-j H:i:s")
            );
            $this->db->insert('sesion', $arrayCamp);
            ?> <script language="JavaScript" type="text/javascript">
                alert('Las Claves no Coinciden, <strong>Usuario no insertado</strong>');
                window.location = '<?php echo base_url(); ?>usuarios';
            </script>
            <?php
            redirect('usuarios');
        }
    }

    function modificar() {
        $data = array();
//////////////////////////////////////
        $id_cons = $_POST['id_cons'];
        $tipo_ced = $_POST['tipo_ced'];
//////////////////////////////////////////////
        $this->form_validation->set_rules('nombres', 'nombres', 'required|trim|xss_clean|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('cedula', 'cedula', 'required|trim|xss_clean|min_length[6]|max_length[15]');
        $this->form_validation->set_rules('telefono', 'telefono', 'required|trim|xss_clean|min_length[7]|max_length[20]');
        $this->form_validation->set_rules('email', 'email', 'required|trim|xss_clean|valid_email|max_length[20]');
//        $this->form_validation->set_rules('nivel', 'nivel', 'required|trim|xss_clean|min_length[4]|max_length[20]');
//////////// Mensaje de Validación
        $this->form_validation->set_message('valid_email', '<b>El campo %s debe ser valido para Email</b>');
        $this->form_validation->set_message('required', '<b>El campo %s es requerido</b>');
//////////// Seguridad XSS
        $resp_nombres = $this->security->xss_clean($this->input->post('nombres'));
        $resp_cedula = $this->security->xss_clean($this->input->post('cedula'));
        $resp_telefono = $this->security->xss_clean($this->input->post('telefono'));
        $resp_email = $this->security->xss_clean($this->input->post('email'));
//        $resp_password = $this->security->xss_clean($this->input->post('password'));
//        $resp_passwordconf = $this->security->xss_clean($this->input->post('passwordconf'));
//        $resp_nivel = $this->security->xss_clean($this->input->post('nivel'));
////////////
        $arrayCampos = array(
            'email' => $resp_email,
            'nombres' => $resp_nombres,
//            'password' => sha1($resp_password),
            'nombres' => $resp_nombres,
            'cedula' => $tipo_ced . $resp_cedula,
            'telefono' => $resp_telefono,
            'fecha_reg' => date("Y-n-j H:i:s")
//            'nivel' => $resp_nivel
        );
        $this->db->where('id', $id_cons);
        $this->db->update('usuario', $arrayCampos);
////////////////////////// Auditoria
        $arrayCamp = array(
            'ip' => $this->session->userdata('ip_address'),
            'usuario' => $this->session->userdata('email'),
            'navegador' => $this->session->userdata('user_agent'),
            'accion' => "UPDATE Usuario",
            'tiempo_accion' => date("Y-n-j H:i:s")
        );
        $this->db->insert('sesion', $arrayCamp);
        ?>
        $mensaje = 'true';

        <?php
        redirect('usuarios');
    }

    function mod_config() {
        $data = array();
        //////////////////////////////////////
        $id_cons = $_POST['id_cons'];
        //////////////////////////////////////////////
        $this->form_validation->set_rules('nombres', 'nombres', 'required|trim|xss_clean|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('clave', 'clave', 'required|trim|xss_clean');
        $this->form_validation->set_rules('clave2', 'clave1', 'required|trim|xss_clean');
        $this->form_validation->set_rules('telefono', 'telefono', 'required|trim|xss_clean|min_length[7]|max_length[20]');
        //////////// Mensaje de Validación
        $this->form_validation->set_message('valid_email', '<b>El campo %s debe ser valido para Email</b>');
        $this->form_validation->set_message('required', '<b>El campo %s es requerido</b>');
        //////////// Seguridad XSS
        $resp_nombres = $this->security->xss_clean($this->input->post('nombres'));
        $resp_telefono = $this->security->xss_clean($this->input->post('telefono'));
        $resp_password = $this->security->xss_clean($this->input->post('clave'));
        $resp_passwordconf = $this->security->xss_clean($this->input->post('clave2'));
        ////////////
        if ($resp_password == $resp_passwordconf) {
            $arrayCampos = array(
                'nombres' => $resp_nombres,
                'password' => sha1($resp_password),
                'telefono' => $resp_telefono
            );
            $this->db->where('id', $id_cons);
            $this->db->update('usuario', $arrayCampos);
            ////////////////////////// Auditoria
            $arrayCamp = array(
                'ip' => $this->session->userdata('ip_address'),
                'usuario' => $this->session->userdata('email'),
                'navegador' => $this->session->userdata('user_agent'),
                'accion' => "CONFIG Usuario",
                'tiempo_accion' => date("Y-n-j H:i:s")
            );
            $this->db->insert('sesion', $arrayCamp);
        } else {
            $mens_error_clave = 'true';
        }
        redirect('usuarios');
    }

    function eliminar() {
        $data = array();
//////////////////////////////////////
        $id_cons = $_POST['id_cons'];
        $this->db->where('id', $id_cons);
        $this->db->delete('usuario');
////////////////////////// Auditoria
        $arrayCamp = array(
            'ip' => $this->session->userdata('ip_address'),
            'usuario' => $this->session->userdata('email'),
            'navegador' => $this->session->userdata('user_agent'),
            'accion' => "DELETE usuario",
            'tiempo_accion' => date("Y-n-j H:i:s")
        );
        $this->db->insert('sesion', $arrayCamp);
        ?>
        <script language="JavaScript" type="text/javascript">
            alert('El Usuario se ha Eliminado Correctamente');
            //window.location = 'formuemp.php';
        </script>
        <script src="<?php echo base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
        <?php
/////////////////////////
        redirect('usuarios');
    }

}
