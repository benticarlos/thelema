<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pdf_auditoria extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('pdf');
    }

    public function index() {
        if ($this->session->userdata('isLoggedIn')) {
            $user_id = $this->session->userdata('id');
            redirect('usuarios/consultar');
        } else {
            $this->show_login(false);
        }
    }

    function reporte() {
        $this->load->model('Auditoria_model');
        $GLOBALS['title'] = '                                                                                        Auditoria del Sistema';
        $columnas = 6;
        $tableHeader = array(
            array(
                array('WIDTH' => 07, 'TEXT' => iconv('UTF-8', 'windows-1252', 'N°'), 'BG_COLOR' => array(108, 129, 56)),
                array('WIDTH' => 22, 'TEXT' => iconv('UTF-8', 'windows-1252', 'IP'), 'BG_COLOR' => array(108, 129, 56)),
                array('WIDTH' => 50, 'TEXT' => iconv('UTF-8', 'windows-1252', 'Usuario'), 'BG_COLOR' => array(108, 129, 56)),
                array('WIDTH' => 40, 'TEXT' => iconv('UTF-8', 'windows-1252', 'Acción'), 'BG_COLOR' => array(108, 129, 56)),
                array('WIDTH' => 28, 'TEXT' => iconv('UTF-8', 'windows-1252', 'Fecha/Hora'), 'BG_COLOR' => array(108, 129, 56)),
                array('WIDTH' => 110, 'TEXT' => iconv('UTF-8', 'windows-1252', 'Navegador'), 'BG_COLOR' => array(108, 129, 56))
            )
        );
        $auditoria = $this->Auditoria_model->consultar();
        $contador = 0;
        $fill = 0;
        if ($auditoria) {
            foreach ($auditoria as $row) {
                if ($fill == 0) {
                    $data[] = array(
                        array('TEXT' => $contador + 1, 'BG_COLOR' => array(255, 255, 255)),
                        array('T_ALIGN' => 'L', 'TEXT' => iconv('UTF-8', 'windows-1252', $row->ip), 'BG_COLOR' => array(255, 255, 255)),
                        array('T_ALIGN' => 'L', 'TEXT' => iconv('UTF-8', 'windows-1252', $row->usuario), 'BG_COLOR' => array(255, 255, 255)),
                        array('T_ALIGN' => 'L', 'TEXT' => iconv('UTF-8', 'windows-1252', $row->accion), 'BG_COLOR' => array(255, 255, 255)),
                        array('T_ALIGN' => 'C', 'TEXT' => iconv('UTF-8', 'windows-1252', $row->tiempo_accion), 'BG_COLOR' => array(255, 255, 255)),
                        array('T_ALIGN' => 'C', 'TEXT' => iconv('UTF-8', 'windows-1252', $row->navegador), 'BG_COLOR' => array(255, 255, 255))
                    );
                    $contador++;
                    $fill = !$fill;
                } else {
                    $data[] = array(
                        array('TEXT' => $contador + 1, 'BG_COLOR' => array(163, 188, 69)),
                        array('T_ALIGN' => 'L', 'TEXT' => iconv('UTF-8', 'windows-1252', $row->ip), 'BG_COLOR' => array(163, 188, 69)),
                        array('T_ALIGN' => 'L', 'TEXT' => iconv('UTF-8', 'windows-1252', $row->usuario), 'BG_COLOR' => array(163, 188, 69)),
                        array('T_ALIGN' => 'L', 'TEXT' => iconv('UTF-8', 'windows-1252', $row->accion), 'BG_COLOR' => array(163, 188, 69)),
                        array('T_ALIGN' => 'C', 'TEXT' => iconv('UTF-8', 'windows-1252', $row->tiempo_accion), 'BG_COLOR' => array(163, 188, 69)),
                        array('T_ALIGN' => 'C', 'TEXT' => iconv('UTF-8', 'windows-1252', $row->navegador), 'BG_COLOR' => array(163, 188, 69))
                    );
                    $contador++;
                    $fill = !$fill;
                }
            };
            $tableType = array(
                'TB_ALIGN' => 'L', //table align on page
                'L_MARGIN' => 5, //space to the left margin
                'BRD_COLOR' => array(0, 0, 0), //border color
                'BRD_SIZE' => '0.3'   //border size
            );
            $this->pdf->tabla($columnas, $data, ' ', $tableHeader, null, null, $tableType);
            $this->pdf->Header($titulo . ".pdf");
        }
    }

}
