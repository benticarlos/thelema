<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Misioneros extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if (!$this->session->userdata('isLoggedIn')) {
            echo "Error en inicion de Sesión";
            redirect('/login/show_login');
        }
        $this->load->model('Usuarios_model');
        $this->load->model('Misioneros_model');
        $this->load->library('pagination');
    }

    function index() {
        if ($this->session->userdata('isLoggedIn')) {
            $user_id = $this->session->userdata('id');
            redirect('inicio/mostrar');
        } else {
            $this->show_login(false);
        }
    }

    function mostrar() { // show_main
        /* URL a la que se desea agregar la paginación */
        $config['base_url'] = base_url() . '/misioneros/mostrar/';

        /* Obtiene el total de registros a paginar */
        $config['total_rows'] = $datos['total'] = $this->Misioneros_model->total_misioneros();

        /* Obtiene el numero de registros a mostrar por pagina */
        $config['per_page'] = $page = 12;

        /* Indica que segmento de la URL tiene la paginación, por default es 3 */
        $config['uri_segment'] = '10';
        $config['num_links'] = $config['total_rows'] / $page;
        /* Se personaliza la paginación para que se adapte a bootstrap */
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><span>';
        $config['cur_tag_close'] = '<span></span></span></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
//        $config['first_link'] = '«';
        $config['first_link'] = 'Primera';
//        $config['prev_link'] = '‹';
        $config['prev_link'] = 'Anterior';
        $config['last_link'] = 'Última';
//        $config['last_link'] = '»';
//        $config['next_link'] = '›';
        $config['next_link'] = 'Siguiente';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        /* Se inicializa la paginacion */
        $this->pagination->initialize($config);
        ////////////////////////////////////////////////////////////
        $datos['misioneros'] = $this->Misioneros_model->consultar($config['per_page'], $this->uri->segment(3));
        $datos['lumisial'] = $this->Misioneros_model->lumisial_menu();

        ///////////////////////////// VISTA
        $user_id = $this->session->userdata('id');
        $datos['i'] = 0;
        $datos['id'] = $user_id;
        $datos['nombres'] = $this->session->userdata('nombres');
        $datos['nivel'] = $this->session->userdata('nivel');
        $datos['titulo'] = 'Inicio';
        $datos['contenido'] = 'misioneros_view';
        $this->load->view('plantillas/plantilla', $datos);
    }

    function consulta() {
        $id_cons = $this->input->post('id_cons');
        $datos['misionero'] = $this->Misioneros_model->consulta($id_cons);
        $datos['lumisial'] = $this->Misioneros_model->lumisial_menu();
        $datos['hijos'] = $this->Misioneros_model->hijos_cons($id_cons);
        $datos['historial'] = $this->Misioneros_model->historial_cons($id_cons);
        ///////////////////////////// VISTA
        $datos['id'] = $this->session->userdata('id');
        $datos['nombres'] = $this->session->userdata('nombres');
        $datos['nivel'] = $this->session->userdata('nivel');
        $datos['titulo'] = 'Misionero';
        $datos['contenido'] = 'misionero_view';
        $this->load->view('plantillas/plantilla', $datos);
    }

    function agregar() {
        $data = array();
        //////////////////////////////////////////////
        $this->form_validation->set_rules('nombre', 'nombre', 'required|trim|xss_clean|min_length[3]|max_length[150]');
        $this->form_validation->set_rules('apellido', 'apellido', 'required|trim|xss_clean|min_length[3]|max_length[150]');
        $this->form_validation->set_rules('cedula', 'cedula', 'required|trim|xss_clean|min_length[6]|max_length[15]');
        $this->form_validation->set_rules('telefono', 'telefono', 'required|trim|xss_clean|min_length[7]|max_length[20]');
        $this->form_validation->set_rules('email', 'email', 'required|trim|xss_clean|valid_email|max_length[20]');
        $this->form_validation->set_rules('fecha_nac', 'fecha_nac', 'required|trim|xss_clean|max_length[20]');
        $this->form_validation->set_rules('nacionalidad', 'nacionalidad', 'required|trim|xss_clean|max_length[20]');
        $this->form_validation->set_rules('fecha_ingreso', 'fecha_ingreso', 'required|trim|xss_clean|min_length[4]|max_length[20]');
        $this->form_validation->set_rules('lugar_mision', 'lugar_mision', 'required|trim|xss_clean|min_length[3]|max_length[150]');
        $this->form_validation->set_rules('fecha_mision', 'fecha_mision', 'required|trim|xss_clean|max_length[150]');
        $this->form_validation->set_rules('lumisial', 'lumisial', 'required|trim|xss_clean|min_length[3]|max_length[150]');
        //////////// Mensaje de Validación
        $this->form_validation->set_message('valid_email', '<b>El campo %s debe ser valido para Email</b>');
        $this->form_validation->set_message('required', '<b>El campo %s es requerido</b>');
        //////////// Seguridad XSS
        $resp_nombre = $this->security->xss_clean($this->input->post('nombre'));
        $resp_apellido = $this->security->xss_clean($this->input->post('apellido'));
        $resp_cedula = $this->security->xss_clean($this->input->post('cedula'));
        $resp_telefono = $this->security->xss_clean($this->input->post('telefono'));
        $resp_email = $this->security->xss_clean($this->input->post('email'));
        $resp_fecha_nac = $this->security->xss_clean($this->input->post('fecha_nac'));
        $resp_nac = $this->security->xss_clean($this->input->post('nacionalidad'));
        $resp_profesion = $this->security->xss_clean($this->input->post('profesion'));
        $resp_cursos = $this->security->xss_clean($this->input->post('cursos'));
        $resp_edo_civil = $this->security->xss_clean($this->input->post('edo_civil'));
        $resp_idiomas = $this->security->xss_clean($this->input->post('idiomas'));
        $resp_fecha_ing = $this->security->xss_clean($this->input->post('fecha_ingreso'));
        $resp_mision = $this->security->xss_clean($this->input->post('lugar_mision'));
        $resp_fecha_mision = $this->security->xss_clean($this->input->post('fecha_mision'));
        $resp_lumisial = $this->security->xss_clean($this->input->post('lumisial'));
        ////////////
        //$id_lumi = 1;
        $arrayCampos = array(
            'nombre' => $resp_nombre,
            'apellido' => $resp_apellido,
            'cedula' => $resp_cedula,
            'telefono' => $resp_telefono,
            'email' => $resp_email,
            'fecha_nac' => $resp_fecha_nac,
            'nacionalidad' => $resp_nac,
            'profesion' => $resp_profesion,
            'cursos' => $resp_cursos,
            'edo_civil' => $resp_edo_civil,
            'idiomas' => $resp_idiomas,
            'nacionalidad' => $resp_nac,
            'fecha_ingreso' => $resp_fecha_ing,
            'lugar_mision' => $resp_mision,
            'fecha_mision' => $resp_fecha_mision,
            'ungido' => $this->input->post('ungido'),
            'lumisial_id' => $resp_lumisial
        );
        $this->db->insert('misionero', $arrayCampos);
////////////////////////// Auditoria
        $arrayCamp = array(
            'ip' => $this->session->userdata('ip_address'),
            'usuario' => $this->session->userdata('email'),
            'navegador' => $this->session->userdata('user_agent'),
            'accion' => "INSERT Misionero",
            'tiempo_accion' => date("Y-n-j H:i:s")
        );
        $this->db->insert('sesion', $arrayCamp);
        /////////////////////////
        redirect('inicio/mostrar');
    }

    function modificar() {
        $data = array();
        $id_cons = $this->input->post('id_cons');
        //////////////////////////////////////////////
        $this->form_validation->set_rules('nombre', 'nombre', 'required|trim|xss_clean|min_length[3]|max_length[150]');
        $this->form_validation->set_rules('apellido', 'apellido', 'required|trim|xss_clean|min_length[3]|max_length[150]');
        $this->form_validation->set_rules('cedula', 'cedula', 'required|trim|xss_clean|min_length[6]|max_length[15]');
        $this->form_validation->set_rules('telefono', 'telefono', 'required|trim|xss_clean|min_length[7]|max_length[20]');
        $this->form_validation->set_rules('email', 'email', 'required|trim|xss_clean|valid_email|max_length[20]');
        $this->form_validation->set_rules('fecha_nac', 'fecha_nac', 'required|trim|xss_clean|max_length[20]');
        $this->form_validation->set_rules('nacionalidad', 'nacionalidad', 'required|trim|xss_clean|max_length[20]');
        $this->form_validation->set_rules('fecha_ingreso', 'fecha_ingreso', 'required|trim|xss_clean|min_length[4]|max_length[20]');
        $this->form_validation->set_rules('lugar_mision', 'lugar_mision', 'required|trim|xss_clean|min_length[3]|max_length[150]');
        $this->form_validation->set_rules('fecha_mision', 'fecha_mision', 'required|trim|xss_clean|max_length[150]');
        $this->form_validation->set_rules('lumisial', 'lumisial', 'required|trim|xss_clean|min_length[3]|max_length[150]');
        //////////// Mensaje de Validación
        $this->form_validation->set_message('valid_email', '<b>El campo %s debe ser valido para Email</b>');
        $this->form_validation->set_message('required', '<b>El campo %s es requerido</b>');
        //////////// Seguridad XSS
        $resp_nombre = $this->security->xss_clean($this->input->post('nombre'));
        $resp_apellido = $this->security->xss_clean($this->input->post('apellido'));
        $resp_cedula = $this->security->xss_clean($this->input->post('cedula'));
        $resp_telefono = $this->security->xss_clean($this->input->post('telefono'));
        $resp_email = $this->security->xss_clean($this->input->post('email'));
        $resp_fecha_nac = $this->security->xss_clean($this->input->post('fecha_nac'));
        $resp_nac = $this->security->xss_clean($this->input->post('nacionalidad'));
        $resp_profesion = $this->security->xss_clean($this->input->post('profesion'));
        $resp_cursos = $this->security->xss_clean($this->input->post('cursos'));
        $resp_edo_civil = $this->security->xss_clean($this->input->post('edo_civil'));
        $resp_idiomas = $this->security->xss_clean($this->input->post('idiomas'));
        $resp_fecha_ing = $this->security->xss_clean($this->input->post('fecha_ingreso'));
        $resp_mision = $this->security->xss_clean($this->input->post('lugar_mision'));
        $resp_fecha_mision = $this->security->xss_clean($this->input->post('fecha_mision'));
        ////////////
        $id_lumi = 1;
        $arrayCampos = array(
            'nombre' => $resp_nombre,
            'apellido' => $resp_apellido,
            'cedula' => $resp_cedula,
            'telefono' => $resp_telefono,
            'email' => $resp_email,
            'fecha_nac' => $resp_fecha_nac,
            'nacionalidad' => $resp_nac,
            'profesion' => $resp_profesion,
            'cursos' => $resp_cursos,
            'edo_civil' => $resp_edo_civil,
            'idiomas' => $resp_idiomas,
            'fecha_ingreso' => $resp_fecha_ing,
            'lugar_mision' => $resp_mision,
            'fecha_mision' => $resp_fecha_mision,
            'ungido' => $this->input->post('ungido'),
            'lumisial_id' => $this->input->post('lumisial')
        );
        $this->db->where('id', $id_cons)
                ->update('misionero', $arrayCampos);
        ////////////////////////// Auditoria
        $arrayCamp = array(
            'ip' => $this->session->userdata('ip_address'),
            'usuario' => $this->session->userdata('email'),
            'navegador' => $this->session->userdata('user_agent'),
            'accion' => "UPDATE Misionero",
            'tiempo_accion' => date("Y-n-j H:i:s")
        );
        $this->db->insert('sesion', $arrayCamp);
        //////////////////////////////
        redirect('inicio/mostrar');
    }

    function eliminar() {
        $data = array();
//////////////////////////////////////
        $id_cons = $_POST['id_cons'];
        $this->db->where('id', $id_cons);
        $this->db->delete('misionero');
        ////////////////////////// Auditoria
        $arrayCamp = array(
            'ip' => $this->session->userdata('ip_address'),
            'usuario' => $this->session->userdata('email'),
            'navegador' => $this->session->userdata('user_agent'),
            'accion' => "DELETE Misionero",
            'tiempo_accion' => date("Y-n-j H:i:s")
        );
        $this->db->insert('sesion', $arrayCamp);
        //////////////////////////////
        redirect('inicio/mostrar');
    }

}
