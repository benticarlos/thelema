<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cursos_misioneros extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if (!$this->session->userdata('isLoggedIn')) {
            echo "Error en inicion de Sesión";
            redirect('/login/show_login');
        }
        $this->load->model('Usuarios_model');
        $this->load->model('Misioneros_model');
        $this->load->library('pagination');
    }

    function index() {
        if ($this->session->userdata('isLoggedIn')) {
            $user_id = $this->session->userdata('id');
            redirect('cursos_misioneros/mostrar');
        } else {
            $this->show_login(false);
        }
    }

    function mostrar() {
        /* URL a la que se desea agregar la paginación */
        $config['base_url'] = base_url() . '/cursos_misioneros/mostrar/';

        /* Obtiene el total de registros a paginar */
        $config['total_rows'] = $this->Misioneros_model->total_cursos_misioneros();

        /* Obtiene el numero de registros a mostrar por pagina */
        $config['per_page'] = $page = 12;

        /* Indica que segmento de la URL tiene la paginación, por default es 3 */
        $config['uri_segment'] = '12';
        $config['num_links'] = $config['total_rows'] / $page;
        /* Se personaliza la paginación para que se adapte a bootstrap */
        $config['full_tag_open'] = '<ul class="pagination pagination-lg">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><span>';
        $config['cur_tag_close'] = '<span></span></span></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
//        $config['first_link'] = '«';
        $config['first_link'] = 'Primera';
//        $config['prev_link'] = '‹';
        $config['prev_link'] = 'Anterior';
        $config['last_link'] = 'Última';
//        $config['last_link'] = '»';
//        $config['next_link'] = '›';
        $config['next_link'] = 'Siguiente';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        /* Se inicializa la paginacion */
        $this->pagination->initialize($config);
        ////////////////////////////////////////////////////////////
        $datos['cursos'] = $this->Misioneros_model->cursos_misioneros($config['per_page'], $this->uri->segment(3));
        ////////////////////////// Auditoria
        $arrayCamp = array(
            'ip' => $this->session->userdata('ip_address'),
            'usuario' => $this->session->userdata('email'),
            'navegador' => $this->session->userdata('user_agent'),
            'accion' => "CONSULTA Cursos Misioneros",
            'tiempo_accion' => date("Y-n-j H:i:s")
        );
        $this->db->insert('sesion', $arrayCamp);
        ////////////////////// VISTA
        $datos['i'] = 0;
        $datos['id'] = $this->session->userdata('id');
        ;
        $datos['nombres'] = $this->session->userdata('nombres');
        $datos['nivel'] = $this->session->userdata('nivel');
        $datos['titulo'] = 'Cursos Misioneros';
        $datos['contenido'] = 'cursos_misioneros_view';
        $this->load->view('plantillas/plantilla', $datos);
    }

    function agregar() {
        $data = array();
        //////////////////////////////////////////////
        $this->form_validation->set_rules('nombre', 'nombre', 'required|trim|xss_clean|min_length[3]|max_length[150]');
        $this->form_validation->set_rules('descripcion', 'descripcion', 'required|trim|xss_clean|min_length[6]|max_length[150]');
        //////////// Mensaje de Validación
        $this->form_validation->set_message('required', '<b>El campo %s es requerido</b>');
        //////////// Seguridad XSS
        $resp_nombre = $this->security->xss_clean($this->input->post('nombre'));
        $resp_descripcion = $this->security->xss_clean($this->input->post('descripcion'));
        ////////////
        $arrayCampos = array(
            'nombre' => $resp_nombre,
            'descripcion' => $resp_descripcion
        );
        $this->db->insert('cursos', $arrayCampos);
        ////////////////////////// Auditoria
        $arrayCamp = array(
            'ip' => $this->session->userdata('ip_address'),
            'usuario' => $this->session->userdata('email'),
            'navegador' => $this->session->userdata('user_agent'),
            'accion' => "INSERT Cursos",
            'tiempo_accion' => date("Y-n-j H:i:s")
        );
        $this->db->insert('sesion', $arrayCamp);
        /////////////////////////
        redirect('cursos/mostrar');
    }

    function modificar() {
        $data = array();
        $id_cons = $this->input->post('id_cons');
        //////////////////////////////////////////////
        $this->form_validation->set_rules('nombre', 'nombre', 'required|trim|xss_clean|min_length[3]|max_length[150]');
        $this->form_validation->set_rules('descripcion', 'descripcion', 'required|trim|xss_clean|min_length[6]|max_length[150]');        //////////// Mensaje de Validación
        $this->form_validation->set_message('required', '<b>El campo %s es requerido</b>');
        //////////// Seguridad XSS
        $resp_nombre = $this->security->xss_clean($this->input->post('nombre'));
        $resp_descripcion = $this->security->xss_clean($this->input->post('descripcion'));
        ////////////
        $arrayCampos = array(
            'nombre' => $resp_nombre,
            'descripcion' => $resp_descripcion
        );
        $this->db->where('id', $id_cons);
        $this->db->update('cursos', $arrayCampos);
        ////////////////////////// Auditoria
        $arrayCamp = array(
            'ip' => $this->session->userdata('ip_address'),
            'usuario' => $this->session->userdata('email'),
            'navegador' => $this->session->userdata('user_agent'),
            'accion' => "UPDATE Cursos",
            'tiempo_accion' => date("Y-n-j H:i:s")
        );
        $this->db->insert('sesion', $arrayCamp);
        //////////////////////////////
        redirect('cursos/mostrar');
    }

    function consulta() {
        $id_cons = $this->input->post('id_cons');
        $datos['cursos'] = $this->Misioneros_model->cursos_cons($id_cons);
        ///////////////////////////// VISTA
        $datos['id'] = $this->session->userdata('id');
        ;
        $datos['nombres'] = $this->session->userdata('nombres');
        $datos['nivel'] = $this->session->userdata('nivel');
        $datos['titulo'] = 'Lumisial';
        $datos['contenido'] = 'cursos_cons_view';
        $this->load->view('plantillas/plantilla', $datos);
    }

    function eliminar() {
        $data = array();
//////////////////////////////////////
        $id_cons = $_POST['id_cons'];
        $this->db->where('id', $id_cons);
        $this->db->delete('lumisial');
        ////////////////////////// Auditoria
        $arrayCamp = array(
            'ip' => $this->session->userdata('ip_address'),
            'usuario' => $this->session->userdata('email'),
            'navegador' => $this->session->userdata('user_agent'),
            'accion' => "DELETE Lumisial",
            'tiempo_accion' => date("Y-n-j H:i:s")
        );
        $this->db->insert('sesion', $arrayCamp);
        //////////////////////////////
        redirect('lumisial/listado');
    }

}
