<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Historial extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if (!$this->session->userdata('isLoggedIn')) {
            echo "Error en inicion de Sesión";
            redirect('/login/show_login');
        }
        $this->load->model('Usuarios_model');
        $this->load->model('Misioneros_model');
        $this->load->library('pagination');
    }

    function index() {
        if ($this->session->userdata('isLoggedIn')) {
            $user_id = $this->session->userdata('id');
            redirect('historial/mostrar');
        } else {
            $this->show_login(false);
        }
    }

    function mostrar() {
        /* URL a la que se desea agregar la paginación */
        $config['base_url'] = base_url() . '/historial/mostrar/';

        /* Obtiene el total de registros a paginar */
        $config['total_rows'] = $this->Misioneros_model->total_historial();

        /* Obtiene el numero de registros a mostrar por pagina */
        $config['per_page'] = $page = 12;

        /* Indica que segmento de la URL tiene la paginación, por default es 3 */
        $config['uri_segment'] = '10';
        $config['num_links'] = $config['total_rows'] / $page;
        /* Se personaliza la paginación para que se adapte a bootstrap */
        $config['full_tag_open'] = '<ul class="pagination pagination-lg">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><span>';
        $config['cur_tag_close'] = '<span></span></span></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
//        $config['first_link'] = '«';
        $config['first_link'] = 'Primera';
//        $config['prev_link'] = '‹';
        $config['prev_link'] = 'Anterior';
        $config['last_link'] = 'Última';
//        $config['last_link'] = '»';
//        $config['next_link'] = '›';
        $config['next_link'] = 'Siguiente';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        /* Se inicializa la paginacion */
        $this->pagination->initialize($config);
        ////////////////////////////////////////////////////////////
        $datos['historial'] = $this->Misioneros_model->historial($config['per_page'], $this->uri->segment(3));
        $datos['misionero'] = $this->Misioneros_model->misionero_menu();
        ////////////////////////// Auditoria
        $arrayCamp = array(
            'ip' => $this->session->userdata('ip_address'),
            'usuario' => $this->session->userdata('email'),
            'navegador' => $this->session->userdata('user_agent'),
            'accion' => "CONSULTA Historial",
            'tiempo_accion' => date("Y-n-j H:i:s")
        );
        $this->db->insert('sesion', $arrayCamp);
        /////////////////////////
        ///////////////////////////// VISTA
        $datos['i'] = 0;
        $datos['id'] = $this->session->userdata('id');
        $datos['nombres'] = $this->session->userdata('nombres');
        $datos['nivel'] = $this->session->userdata('nivel');
        $datos['titulo'] = 'Historial Misionero';
        $datos['contenido'] = 'historial_view';
        $this->load->view('plantillas/plantilla', $datos);
    }

    function agregar() {
        $data = array();
        //////////////////////////////////////////////
        $this->form_validation->set_rules('nombre', 'nombre', 'required|trim|xss_clean|min_length[3]|max_length[150]');
        $this->form_validation->set_rules('apellido', 'apellido', 'required|trim|xss_clean|min_length[3]|max_length[150]');
        $this->form_validation->set_rules('fecha_nac', 'fecha_nac', 'required|trim|xss_clean|min_length[7]|max_length[20]');
        //////////// Mensaje de Validación
        $this->form_validation->set_message('required', '<b>El campo %s es requerido</b>');
        //////////// Seguridad XSS
        $resp_nombre = $this->security->xss_clean($this->input->post('nombre'));
        $resp_apellido = $this->security->xss_clean($this->input->post('apellido'));
        $resp_fecha_nac = $this->security->xss_clean($this->input->post('fecha_nac'));
        $resp_misionero = $this->security->xss_clean($this->input->post('misionero'));
        ////////////
        $arrayCampos = array(
            'nombreh' => $resp_nombre,
            'apellidoh' => $resp_apellido,
            'fecha_nac' => $resp_fecha_nac,
            'misionero_id' => $resp_misionero
        );
        $this->db->insert('hijos', $arrayCampos);
        ////////////////////////// Auditoria
        $arrayCamp = array(
            'ip' => $this->session->userdata('ip_address'),
            'usuario' => $this->session->userdata('email'),
            'navegador' => $this->session->userdata('user_agent'),
            'accion' => "INSERT Historial",
            'tiempo_accion' => date("Y-n-j H:i:s")
        );
        $this->db->insert('sesion', $arrayCamp);
        /////////////////////////
        redirect('hijos/mostrar');
    }

    function misionero() {
        $data = array();
        //////////////////////////////////////////////
        $this->form_validation->set_rules('lugar_misionh', 'lugar_misionh', 'required|trim|xss_clean|min_length[3]|max_length[150]');
        $this->form_validation->set_rules('fecha_inicio', 'fecha_inicio', 'required|trim|xss_clean|min_length[3]|max_length[150]');
        $this->form_validation->set_rules('fecha_fin', 'fecha_fin', 'required|trim|xss_clean|min_length[7]|max_length[20]');
        //////////// Mensaje de Validación
        $this->form_validation->set_message('required', '<b>El campo %s es requerido</b>');
        //////////// Seguridad XSS
        $resp_lugar_misionh = $this->security->xss_clean($this->input->post('lugar_misionh'));
        $resp_fecha_inicio = $this->security->xss_clean($this->input->post('fecha_inicio'));
        $resp_fecha_fin = $this->security->xss_clean($this->input->post('fecha_fin'));
        $resp_misionero = $this->security->xss_clean($this->input->post('misionero'));
        ////////////
        $arrayCampos = array(
            'lugar_misionh' => $resp_lugar_misionh,
            'fecha_inicio' => $resp_fecha_inicio,
            'fecha_fin' => $resp_fecha_fin,
            'misionero_id' => $resp_misionero
        );
        $this->db->insert('historial', $arrayCampos);
        ////////////////////////// Auditoria
        $arrayCamp = array(
            'ip' => $this->session->userdata('ip_address'),
            'usuario' => $this->session->userdata('email'),
            'navegador' => $this->session->userdata('user_agent'),
            'accion' => "INSERT Historial Misionero",
            'tiempo_accion' => date("Y-n-j H:i:s")
        );
        $this->db->insert('sesion', $arrayCamp);
        ///////////////////////////// VISTA
        $datos['id_cons'] = $this->input->post('id_cons');
        $datos['i'] = 0;
        $datos['id'] = $this->session->userdata('id');
        $datos['nombres'] = $this->session->userdata('nombres');
        $datos['nivel'] = $this->session->userdata('nivel');
        $datos['titulo'] = 'Hijo Insertado';
        $datos['contenido'] = 'historial_misionero_view';
        $this->load->view('plantillas/plantilla', $datos);
    }

    function modificar() {
        $data = array();
        $id_cons = $this->input->post('id_cons');
        //////////////////////////////////////////////
        $this->form_validation->set_rules('lumisial', 'lumisial', 'required|trim|xss_clean|min_length[2]|max_length[150]');
        $this->form_validation->set_rules('poblado', 'poblado', 'required|trim|xss_clean|min_length[2]|max_length[150]');
        $this->form_validation->set_rules('direccion', 'direccion', 'required|trim|xss_clean|min_length[7]|max_length[20]');
        $this->form_validation->set_rules('diocesis_id', 'diocesis_id', 'required|trim|xss_clean|valid_email|max_length[20]');
        //////////// Mensaje de Validación
        $this->form_validation->set_message('required', '<b>El campo %s es requerido</b>');
        //////////// Seguridad XSS
        $resp_lumisial = $this->security->xss_clean($this->input->post('lumisial'));
        $resp_poblado = $this->security->xss_clean($this->input->post('poblado'));
        $resp_direccion = $this->security->xss_clean($this->input->post('direccion'));
        ////////////
        $arrayCampos = array(
            'lumisial' => $resp_lumisial,
            'poblado' => $resp_poblado,
            'direccion' => $resp_direccion,
            'diocesis_id' => $this->input->post('diocesis')
        );
        $this->db->where('id', $id_cons)
                ->update('historial', $arrayCampos);
        ////////////////////////// Auditoria
        $arrayCamp = array(
            'ip' => $this->session->userdata('ip_address'),
            'usuario' => $this->session->userdata('email'),
            'navegador' => $this->session->userdata('user_agent'),
            'accion' => "UPDATE Historial",
            'tiempo_accion' => date("Y-n-j H:i:s")
        );
        $this->db->insert('sesion', $arrayCamp);
        //////////////////////////////
        redirect('lumisial/mostrar');
    }

    function consulta() {
        $id_cons = $this->input->post('id_cons');
        $datos['lum'] = $this->Misioneros_model->lumisial_cons($id_cons);
        $datos['lumisial'] = $this->Misioneros_model->lumisial_menu();
        $datos['diocesis'] = $this->Misioneros_model->diocesis_menu();
        ///////////////////////////// VISTA
        $datos['id'] = $this->session->userdata('id');
        $datos['nombres'] = $this->session->userdata('nombres');
        $datos['nivel'] = $this->session->userdata('nivel');
        $datos['titulo'] = 'Lumisial';
        $datos['contenido'] = 'lumisial_cons_view';
        $this->load->view('plantillas/plantilla', $datos);
    }

    function eliminar() {
        $data = array();
//////////////////////////////////////
        $id_cons = $_POST['id_cons'];
        $this->db->where('id', $id_cons);
        $this->db->delete('lumisial');
        ////////////////////////// Auditoria
        $arrayCamp = array(
            'ip' => $this->session->userdata('ip_address'),
            'usuario' => $this->session->userdata('email'),
            'navegador' => $this->session->userdata('user_agent'),
            'accion' => "DELETE Historial",
            'tiempo_accion' => date("Y-n-j H:i:s")
        );
        $this->db->insert('sesion', $arrayCamp);
        //////////////////////////////
        redirect('lumisial/mostrar');
    }

}
