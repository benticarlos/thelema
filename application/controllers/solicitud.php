<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Solicitud extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Solicitud_model');
//        $this->load->model('Usuarios_model');
    }

    function index() {
        if ($this->session->userdata('isLoggedIn')) {
            $user_id = $this->session->userdata('id');
            redirect('inicio/mostrar');
        } else {
            $this->show_login(false);
        }
    }

    function mostrar() {
        $datos['solicitud'] = $this->Solicitud_model->consultar_todos();
        ///////////////////////////// VISTA
        $datos['i'] = 0;
        $datos['id'] = $this->session->userdata('id');
        $datos['nombres'] = $this->session->userdata('nombres');
        $datos['nivel'] = $this->session->userdata('nivel');
        $datos['titulo'] = 'Solicitudes';
        $datos['contenido'] = 'solicitud_view';
        $this->load->view('plantillas/plantilla', $datos);
        ///////////////////////// Auditoria
        $arrayCamp = array(
            'ip' => $this->session->userdata('ip_address'),
            'usuario' => $this->session->userdata('email'),
            'navegador' => $this->session->userdata('user_agent'),
            'accion' => "CONSULTA Solicitud",
            'tiempo_accion' => date("Y-n-j H:i:s")
        );
        $this->db->insert('sesion', $arrayCamp);
    }

    function insertar() {
        $data = array();
        //////////////////////////////////////////////         
        $this->form_validation->set_rules('asunto', 'asunto', 'required|trim|xss_clean|max_length[250]');
        $this->form_validation->set_rules('contenido', 'contenido', 'required|trim|xss_clean');
        //////////// Mensaje de Validación        
        $this->form_validation->set_message('required', '<b>El campo %s es requerido</b>');
        //////////// Seguridad XSS
        $resp_asunto = $this->security->xss_clean($this->input->post('asunto'));
        $resp_contenido = $this->security->xss_clean($this->input->post('contenido'));
        /////////// 
        $arrayCampos = array(
            'asunto' => $resp_asunto,
            'contenido' => $resp_contenido,
            'fecha' => date("Y-n-j"),
            'autorizado' => 0,
            'id_usuario' => $this->input->post('id')
        );
        $this->db->insert('solicitud', $arrayCampos);
////////////////////////// Auditoria
        $arrayCamp = array(
            'ip' => $this->session->userdata('ip_address'),
            'usuario' => $this->session->userdata('email'),
            'navegador' => $this->session->userdata('user_agent'),
            'accion' => "INSERT Solicitud",
            'tiempo_accion' => date("Y-n-j H:i:s")
        );
        $this->db->insert('sesion', $arrayCamp);
        ?>
        <script language="JavaScript" type="text/javascript">
            alert('La Solicitud ha sido Insertada Correctamente');
            //window.location = 'foo.php';
        </script>
        <?php

        redirect('inicio');
    }

    function consulta() {
        $id_consu = $this->input->post('id_consu');
        $datos['i_cons'] = $this->input->post('i_cons');
        $datos['solicitud'] = $this->Solicitud_model->consulta($id_consu);

///////////////////////////// VISTA
        $datos['nombres'] = $this->session->userdata('nombres');
        $datos['id'] = $this->session->userdata('id');
        $datos['nivel'] = $this->session->userdata('nivel');
        $datos['titulo'] = 'Consulta de Solicitud';
        $datos['contenido'] = 'solicitud_cons_view';
        $this->load->view('plantillas/plantilla', $datos);
////////////////////////// Auditoria
        $arrayCamp = array(
            'ip' => $this->session->userdata('ip_address'),
            'usuario' => $this->session->userdata('email'),
            'navegador' => $this->session->userdata('user_agent'),
            'accion' => "CONSULTA consumo",
            'tiempo_accion' => date("Y-n-j H:i:s")
        );
    }

}
