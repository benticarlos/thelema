<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Imprimir extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('pdf');
    }

    public function index() {
        if ($this->session->userdata('isLoggedIn')) {
            $user_id = $this->session->userdata('id');
            redirect('usuarios/consultar');
        } else {
            $this->show_login(false);
        }
    }

    function usuarios() {
        $this->load->model('Usuarios_model');
        $GLOBALS['title'] = '                                                                                              Listado de Usuarios';
        $columnas = 8;
        $tableHeader = array(
            array(
                array('WIDTH' => 07, 'TEXT' => iconv('UTF-8', 'windows-1252', 'N°'), 'BG_COLOR' => array(108, 129, 56)),
                array('WIDTH' => 50, 'TEXT' => iconv('UTF-8', 'windows-1252', 'Usuario / Email'), 'BG_COLOR' => array(108, 129, 56)),
                array('WIDTH' => 30, 'TEXT' => iconv('UTF-8', 'windows-1252', 'Nombre y Apellido'), 'BG_COLOR' => array(108, 129, 56)),
                array('WIDTH' => 20, 'TEXT' => iconv('UTF-8', 'windows-1252', 'Cédula'), 'BG_COLOR' => array(108, 129, 56)),
                array('WIDTH' => 80, 'TEXT' => iconv('UTF-8', 'windows-1252', 'Institución'), 'BG_COLOR' => array(108, 129, 56)),
                array('WIDTH' => 20, 'TEXT' => iconv('UTF-8', 'windows-1252', 'Teléfono'), 'BG_COLOR' => array(108, 129, 56)),
                array('WIDTH' => 30, 'TEXT' => iconv('UTF-8', 'windows-1252', 'Fecha de Registro'), 'BG_COLOR' => array(108, 129, 56)),
                array('WIDTH' => 25, 'TEXT' => iconv('UTF-8', 'windows-1252', 'Nivel'), 'BG_COLOR' => array(108, 129, 56)),
            )
        );
        $usuarios = $this->Usuarios_model->consultar();
        $contador = 0;
        $fill = 0;
        if ($usuarios) {
            foreach ($usuarios as $row) {
                if ($row->nivel == 1)
                    $univel = $row->nivel . "-Administrador";
                if ($row->nivel == 2)
                    $univel = $row->nivel . "-Supervisor";
                if ($row->nivel == 3)
                    $univel = $row->nivel . "-Normal";
                if ($row->nivel == 4)
                    $univel = $row->nivel . "-Consulta";
                if ($fill == 0) {
                    $data[] = array(
                        array('TEXT' => $contador + 1, 'BG_COLOR' => array(255, 255, 255)),
                        array('T_ALIGN' => 'L', 'TEXT' => iconv('UTF-8', 'windows-1252', $row->email), 'BG_COLOR' => array(255, 255, 255)),
                        array('T_ALIGN' => 'L', 'TEXT' => iconv('UTF-8', 'windows-1252', $row->nombres), 'BG_COLOR' => array(255, 255, 255)),
                        array('T_ALIGN' => 'L', 'TEXT' => iconv('UTF-8', 'windows-1252', $row->cedula), 'BG_COLOR' => array(255, 255, 255)),
                        array('T_ALIGN' => 'L', 'TEXT' => iconv('UTF-8', 'windows-1252', $row->institucion), 'BG_COLOR' => array(255, 255, 255)),
                        array('T_ALIGN' => 'C', 'TEXT' => iconv('UTF-8', 'windows-1252', $row->telefono), 'BG_COLOR' => array(255, 255, 255)),
                        array('T_ALIGN' => 'L', 'TEXT' => iconv('UTF-8', 'windows-1252', $row->fecha_reg), 'BG_COLOR' => array(255, 255, 255)),
                        array('T_ALIGN' => 'L', 'TEXT' => iconv('UTF-8', 'windows-1252', $univel), 'BG_COLOR' => array(255, 255, 255)),
                    );
                    $contador++;
                    $fill = !$fill;
                } else {
                    $data[] = array(
                        array('TEXT' => $contador + 1, 'BG_COLOR' => array(163, 188, 69)),
                        array('T_ALIGN' => 'L', 'TEXT' => iconv('UTF-8', 'windows-1252', $row->email), 'BG_COLOR' => array(163, 188, 69)),
                        array('T_ALIGN' => 'L', 'TEXT' => iconv('UTF-8', 'windows-1252', $row->nombres), 'BG_COLOR' => array(163, 188, 69)),
                        array('T_ALIGN' => 'L', 'TEXT' => iconv('UTF-8', 'windows-1252', $row->cedula), 'BG_COLOR' => array(163, 188, 69)),
                        array('T_ALIGN' => 'L', 'TEXT' => iconv('UTF-8', 'windows-1252', $row->institucion), 'BG_COLOR' => array(163, 188, 69)),
                        array('T_ALIGN' => 'C', 'TEXT' => iconv('UTF-8', 'windows-1252', $row->telefono), 'BG_COLOR' => array(163, 188, 69)),
                        array('T_ALIGN' => 'L', 'TEXT' => iconv('UTF-8', 'windows-1252', $row->fecha_reg), 'BG_COLOR' => array(163, 188, 69)),
                        array('T_ALIGN' => 'L', 'TEXT' => iconv('UTF-8', 'windows-1252', $univel), 'BG_COLOR' => array(163, 188, 69)),
                    );
                    $contador++;
                    $fill = !$fill;
                }
            };
            $tableType = array(
                'TB_ALIGN' => 'L', //table align on page
                'L_MARGIN' => 5, //space to the left margin
                'BRD_COLOR' => array(0, 0, 0), //border color
                'BRD_SIZE' => '0.3'   //border size
            );
            $this->pdf->tabla($columnas, $data, ' ', $tableHeader, null, null, $tableType);
            $this->pdf->Header($titulo.".pdf");
        }
    }

}
