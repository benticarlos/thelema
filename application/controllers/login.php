<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    function index() {
        if ($this->session->userdata('isLoggedIn')) {
            redirect('inicio/mostrar');
        } else {
            $this->show_login(false);
        }
    }

    function show_login($show_error = false) {
        $datos['error'] = $show_error;
/////////////////////////////////////////////
        $datos['titulo'] = 'Login';
        $datos['contenido'] = 'login_view';
        $this->load->view('plantillas/plantilla', $datos);
    }

    function acceso() { //login_user
        // Create an instance of the user model
        $this->load->model('usuarios_model');

        // Grab the email and password from the form POST
        $email = $this->input->post('email');
        $pass = $this->input->post('password');

        $data = array();
        $this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|trim|xss_clean');
        $this->form_validation->set_message('required', '<b>El campo %s es requerido</b>');
        $resp_email = $this->security->xss_clean($this->input->post('email'));
        $resp_password = $this->security->xss_clean($this->input->post('password'));
        //Ensure values exist for email and pass, and validate the user's credentials
//        if ($email && $pass && $this->usuarios_model->validate_user($email, $password)) {
        if ($email && $pass && $this->usuarios_model->validate_user($resp_email, $resp_password)) {
            // If the user is valid, redirect to the main view
            redirect('inicio/mostrar');
        } else {
            ////////////////////////// Auditoria
            $arrayCamp = array(
                'ip' => $this->session->userdata('ip_address'),
                'usuario' => $resp_email,
                'navegador' => $this->session->userdata('user_agent'),
                'accion' => "ENTRADA FALLIDA",
                'tiempo_accion' => date("Y-n-j H:i:s")
            );
            $this->db->insert('sesion', $arrayCamp);
            ///////////////////////////////////////////////////
            // Otherwise show the login screen with an error message.
            $this->show_login(true);
        }
    }

}
