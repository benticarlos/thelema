<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Inicio extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('isLoggedIn')) {
            redirect('/login/show_login');
        }
        $this->load->model('Misioneros_model');
        $this->load->model('Usuarios_model');
    }

    function index() {
        if ($this->session->userdata('isLoggedIn')) {
            $user_id = $this->session->userdata('id');
            redirect('inicio/mostrar');
        } else {
            $this->show_login(false);
        }
    }

    function mostrar() { // show_main
        ////////////////// Auditoria
        $user_id = $this->session->userdata('id');
        $nivel = $this->session->userdata('nivel');
        $nombres = $this->session->userdata('nombres');
        $arrayCampos = array(
            'ip' => $this->session->userdata('ip_address'),
            'usuario' => $this->session->userdata('email'),
            'navegador' => $this->session->userdata('user_agent'),
            'accion' => "ENTRADA",
            'tiempo_accion' => date("Y-n-j H:i:s")
        );
        $this->db->insert('sesion', $arrayCampos);
        redirect('misioneros/mostrar');
    }

    function logout() {
        /////////////////////////////////// Auditoria
        $user_id = $this->session->userdata('id');
        $arrayCampos = array(
            'ip' => $this->session->userdata('ip_address'),
            'usuario' => $this->session->userdata('email'),
            'navegador' => $this->session->userdata('user_agent'),
            'accion' => "SALIDA",
            'tiempo_accion' => date("Y-n-j H:i:s")
        );
        $this->db->insert('sesion', $arrayCampos);
        /////////////////////////////////
        $this->session->sess_destroy();
        redirect('inicio');
    }

}
