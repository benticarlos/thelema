<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pdf_consumo_ins extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('pdf');
    }

    public function index() {
        if ($this->session->userdata('isLoggedIn')) {
            $user_id = $this->session->userdata('id');
            redirect('usuarios/consultar');
        } else {
            $this->show_login(false);
        }
    }

    function reporte() {

//        $this->load->model('Consumo_model');
        $GLOBALS['title'] = '                                                                                        Consumo de Energía Eléctrica Insertado';
        $columnas = 3;
        $tableHeader = array(
            array(
                array('WIDTH' => 10, 'TEXT' => iconv('UTF-8', 'windows-1252', 'N°'), 'BG_COLOR' => array(108, 129, 56)),
                array('WIDTH' => 30, 'TEXT' => iconv('UTF-8', 'windows-1252', 'Consumo (Kw/h)'), 'BG_COLOR' => array(108, 129, 56)),
                array('WIDTH' => 30, 'TEXT' => iconv('UTF-8', 'windows-1252', 'Fecha'), 'BG_COLOR' => array(108, 129, 56))
            )
        );
        $id_consu = $this->input->post('id_consu');
        $consumo = $this->input->post('consumo');
        $fecha = $this->input->post('fecha');
//        $consumo = $this->Consumo_model->consulta($id_consu);
        $contador = 0;
        $fill = 0;
        if ($consumo) {
            if ($fill == 0) {
                $data[] = array(
                    array('TEXT' => $contador + 1, 'BG_COLOR' => array(255, 255, 255)),
                    array('T_ALIGN' => 'C', 'TEXT' => iconv('UTF-8', 'windows-1252', $consumo), 'BG_COLOR' => array(255, 255, 255)),
                    array('T_ALIGN' => 'C', 'TEXT' => iconv('UTF-8', 'windows-1252', $fecha), 'BG_COLOR' => array(255, 255, 255))
                );
                $contador++;
                $fill = !$fill;
            } 
            $tableType = array(
                'TB_ALIGN' => 'L', //table align on page
                'L_MARGIN' => 35, //space to the left margin
                'BRD_COLOR' => array(0, 0, 0), //border color
                'BRD_SIZE' => '0.3'   //border size
            );
            $this->pdf->tabla($columnas, $data, ' ', $tableHeader, null, null, $tableType);
            $this->pdf->Header($titulo . ".pdf");
        }
    }

}
